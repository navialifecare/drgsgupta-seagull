package com.healthapp.drgsgupta;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthapp.appointment_module.AppointmentDelegate;
import com.healthapp.appointment_module.AppointmentsListCallback;
import com.healthapp.drgsgupta.health_activity.data.ActivityType;
import com.healthapp.drgsgupta.health_activity.data.HealthActivity;
import com.navia.modules.treatmentmodule.TreatmentModuleDelegate;
import com.navia.naviahelp.HelperFunctions;

import java.util.ArrayList;

/**
 * Created by amitgupta on 5/7/18.
 */

public class ViewPagerHealthActivity extends PagerAdapter {
    Activity activity;
    LayoutInflater layoutInflater;
    ArrayList<HealthActivity> arrayListActivities;

    public ViewPagerHealthActivity(Activity activity,ArrayList<HealthActivity>activities) {
        layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        arrayListActivities = activities;
        this.activity =activity;
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View view = layoutInflater.inflate(R.layout.item_upcoming_activity,container,false);
        TextView tv_title = (TextView)view.findViewById(R.id.tv_name);
        TextView tv_date = (TextView)view.findViewById(R.id.tv_date);
        TextView tv_time = (TextView)view.findViewById(R.id.tv_time);
        ImageView img_activity = (ImageView)view.findViewById(R.id.img_activity_type);
        if(arrayListActivities.get(position).getType().equals(ActivityType.TREATMENT)){
            img_activity.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle));
            img_activity.setImageResource(R.drawable.ic_pills2);
            tv_date.setText(arrayListActivities.get(position).getDate()+"");

        }
        else{
            img_activity.setImageResource(R.drawable.ic_calendar7);
            String date = HelperFunctions.
                    convertLongToDate(HelperFunctions.convertDateStringTolong(arrayListActivities.get(position).getDate(),1),
                            3);
            tv_date.setText(date);
        }

        tv_title.setText( arrayListActivities.get(position).getName()+"");
        tv_time.setText(arrayListActivities.get(position).getTime()+"");

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(arrayListActivities.get(position).getType().equals(ActivityType.TREATMENT)) {
                    TreatmentModuleDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).startTreatmentModule(activity, AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER);

                }else{
                    launchAppointmentsAndVisits();
                }
            }
        });


        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return arrayListActivities.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View)object);
    }

    private void launchAppointmentsAndVisits() {
        AppointmentDelegate.getInstance(AppConstants.APP_NAME, AppConstants.VERSION_CODE).showUserAppointmentsList(activity, new AppointmentsListCallback() {
            @Override
            public void showDepartmentListing() {
                /**
                 * The app may have departments, or just doctors, handle that in
                 * Doctor Department module
                 * If app belongs to only one doctor only, show slots directly
                 **/

                AppointmentDelegate.getInstance(AppConstants.APP_NAME,
                        AppConstants.VERSION_CODE)
                        .initiateBookingForDoctor(activity,
                                AppConstants.MEDICAL_DIRECTOR_NAME, AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER, "", "","");
            }

            @Override
            public void startHomeActivity(Context context) {
                Intent  intent=new Intent(context, HomeActivity.class);
                context.startActivity(intent);
            }
        });
    }
}

