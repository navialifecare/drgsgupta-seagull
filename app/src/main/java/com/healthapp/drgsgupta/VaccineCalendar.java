package com.healthapp.drgsgupta;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.healthapp.drgsgupta.Adapter.MyVaccinationListAdapter;
import com.navia.naviahelp.MyPrefs;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by JaiGuruji on 22/02/17.
 */

public class VaccineCalendar extends Fragment {
    FrameLayout mCalendarView;
    CaldroidFragment mCaldroidFragment;
    Activity mActivity;
    JSONArray jsonArray;
    ExpandableListView mExpandableListView;
    ArrayList<JSONObject> jsonObjects ;
    ExpandableListAdapter expandableListAdapter;
    TextView img_empty;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        try {
            jsonArray = new JSONArray(bundle.getString("jsonArray"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_vaccination_calendar,container,false);
       mCalendarView =(FrameLayout)view.findViewById(R.id.calendar_view);
       mCaldroidFragment = new CaldroidFragment();
       mActivity = getActivity();
       Bundle args = new Bundle();
       Calendar calendar = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH,calendar.get(Calendar.MONTH)+1);
        args.putInt(CaldroidFragment.YEAR,calendar.get(Calendar.YEAR));
        FragmentManager fragmentManager = getFragmentManager();
        mCaldroidFragment.setArguments(args);
        fragmentManager.beginTransaction().replace(R.id.calendar_view,mCaldroidFragment).commit();
        mExpandableListView = (ExpandableListView) view.findViewById(R.id.duration_expandable_listview);
        jsonObjects = new ArrayList<>();

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Vaccines");

        getUserActivities();
        return view;
    }

    private void getUserActivities(){

        ProgressDialog progressDialog = ProgressDialog.show(getActivity(),"Fetching","helo",false);

        final JSONArray dateTasksArray = new JSONArray();

        ArrayList<Date> dates=new ArrayList<>();

        final SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyy");
        TreeMap<String,ArrayList<JSONObject>> treeMap = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String lhs,String rhs) {
                try {
                    return simpledateformat.parse(lhs).compareTo(simpledateformat.parse(rhs));
                }catch (Exception e){
                    e.printStackTrace();
                }
                return 0;
            }
        });

        for(int i=0;i<jsonArray.length();i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String time = jsonObject.getString("vaccination_receiving_date");
                String timeArray[] = time.split("-");
                String date = timeArray[0] + "/" + timeArray[1] + "/" + timeArray[2];
                if (treeMap.containsKey(date)) {
                    ArrayList<JSONObject> arrayList = treeMap.get(date);
                    arrayList.add(jsonObject);
                } else {
                    ArrayList<JSONObject> arrayList = new ArrayList<>();
                    arrayList.add(jsonObject);
                    treeMap.put(date, arrayList);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

                try{

                for(String key:treeMap.keySet()){
                   ArrayList<JSONObject>arrayList = treeMap.get(key);
                   JSONArray dateVaccinesArray = new JSONArray();
                    JSONObject vaccineObject = new JSONObject();
                   for(JSONObject jsonObject1:arrayList){
                       JSONObject jsonObject2 = new JSONObject();
                       jsonObject2.put("vaccination_name",jsonObject1.getString("vaccine"));
                       jsonObject2.put("due_time",jsonObject1.getString("vaccination_receiving_date"));
                       jsonObject2.put("dosage", "1");
                       jsonObject2.put("taken_status",getStatusForVaccine(jsonObject1.getString("done")));
                       jsonObject2.put("color",colorForStatus(getStatusForVaccine(jsonObject1.getString("done"))));
                       dateVaccinesArray.put(jsonObject2);
                   }
                    vaccineObject.put("tasks",dateVaccinesArray);
                    vaccineObject.put("date",key);
                    dateTasksArray.put(vaccineObject);
                    dates.add(simpledateformat.parse(key));
                }
                    if (dates.size() > 0) {
                        Date d = (Date) dates.get(dates.size() - 1);
                        Calendar calendar1 = Calendar.getInstance();
                        calendar1.setTime(d);
                        calendar1.add(Calendar.DATE, 1);
                        d = calendar1.getTime();

                        final CaldroidListener listener = new CaldroidListener() {

                            @Override
                            public void onSelectDate(Date date, View view) {
                                scrollList(date);
                            }

                            @Override
                            public void onChangeMonth(int month, int year) {
                                String text = "month: " + month + " year: " + year;

                            }

                            void scrollList(Date d) {
                                int scroll = (int) getScrollPos(d);
                                int count = expandableListAdapter.getGroupCount();
                                for (int i = 0; i < count; i++)
                                    mExpandableListView.collapseGroup(i);
                                int noOfChild = expandableListAdapter.getChildrenCount(scroll);
                                if (noOfChild != 0) {
                                    mExpandableListView.expandGroup(scroll);
                                    mExpandableListView.smoothScrollToPosition(scroll + noOfChild);
                                }
                            }

                            long getScrollPos(Date clickDate) {
                                expandableListAdapter = mExpandableListView.getExpandableListAdapter();

                                String dateString = (String) expandableListAdapter.getGroup(0); //19/01/2016
                                int yr = Integer.parseInt(dateString.split("/")[2]) - 1900;
                                int mth = Integer.parseInt(dateString.split("/")[1]) - 1;
                                int dt = Integer.parseInt(dateString.split("/")[0]);

                                Date groupTopDate = new Date(yr, mth, dt, 0, 0);

                                long diff = clickDate.getTime() - groupTopDate.getTime();
                                long daysss = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                                Log.e("TAG", "***" + dateString + "**" + daysss);

                                return daysss;
                            }
                        };


                        mCaldroidFragment.setCaldroidListener(listener);
                        int i = 0;
                        for (Date dd : dates) {
                            try {
                                JSONObject entity = dateTasksArray.getJSONObject(i).getJSONArray("tasks").getJSONObject(0);
                                if (entity.getString("color").equals("green")) {
                                    mCaldroidFragment.setBackgroundResourceForDate(R.color.green_light, dd);
                                } else if (entity.getString("color").equals("red")) {
                                    mCaldroidFragment.setBackgroundResourceForDate(R.color.red_light, dd);
                                } else if (entity.getString("color").equals("yellow")) {
                                    mCaldroidFragment.setBackgroundResourceForDate(R.color.yellow, dd);
                                } else {
                                    mCaldroidFragment.setBackgroundResourceForDate(R.color.caldroid_holo_blue_light, dd);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            i++;
                        }

                        mCaldroidFragment.refreshView();

                    } else {
                        (new android.app.AlertDialog.Builder(mActivity)).setMessage((new StringBuilder()).
                                append("Sorry, we have no information for treatment ")
                                .append(MyPrefs.getString(mActivity, "treatment_name", ""))
                                .toString()).setPositiveButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialoginterface, int i) {
                                    }
                                }).show();
                    }
                    if (dateTasksArray.length() > 0) {
                        //Log.d("TAG", "Data : " + dateTasksArray);
                        MyVaccinationListAdapter adapter = new MyVaccinationListAdapter(getActivity(), dateTasksArray);
                        mExpandableListView.setAdapter(adapter);
                        if (!adapter.isEmpty())
                            mExpandableListView.expandGroup(0);
                        progressDialog.dismiss();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }

    private String colorForStatus(String status){
        if(status.equals("TAKEN")){
            return "green";
        }
        else if(status.equals("MISSED")){
            return "red";
        }
        else{
            return "blue";
        }
    }

    private String getStatusForVaccine(String status){
        if(status.equals("true")){
            return "TAKEN";
        }
        else if(status.equals("false")){
            return "MISSED";
        }
       else{
            return "PENDING";
        }
    }
}
