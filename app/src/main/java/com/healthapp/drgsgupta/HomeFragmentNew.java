package com.healthapp.drgsgupta;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.healthapp.appointment_module.AppointmentsListCallback;
import com.healthapp.drgsgupta.health_activity.data.HealthActivity;
import com.healthapp.drgsgupta.health_activity.ui.UpcomingActivity;
import com.healthapp.appointment_module.AppointmentDelegate;
import com.healthapp.doctor_departments_module.DoctorsDepartmentsDelegate;
import com.healthapp.body_vital_module.BodyVitalDelegate;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.MyPrefs;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.Disposable;

import static com.healthapp.drgsgupta.AppConstants.APP_NAME;
import static com.healthapp.drgsgupta.AppConstants.VERSION_CODE;

public class HomeFragmentNew extends Fragment implements View.OnClickListener{
    Activity mActivity;
    ViewPager viewpagerHealthActivity;
    ProgressBar progressBar;
    RelativeLayout rl_viewpager;
    ViewDot viewDotIndicator;
    TextView tv_label_welcome;
    ProgressDialog loginProgressDialog;
    Boolean isMedicalProfileSkipped=true;
    Disposable disposable;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_home1, container, false);

        mActivity = getActivity();

        RelativeLayout rl_create_treatment = (RelativeLayout) view.findViewById(R.id.rl_treatment);
        RelativeLayout rl_reports = (RelativeLayout) view.findViewById(R.id.rl_upcoming_activities);
        RelativeLayout rl_appointments = (RelativeLayout) view.findViewById(R.id.rl_appointments);
        RelativeLayout rl_chat = (RelativeLayout) view.findViewById(R.id.rl_chat);
        RelativeLayout rl_body_vitals = (RelativeLayout) view.findViewById(R.id.rl_my_profile);
        RelativeLayout rl_doctor_profile = (RelativeLayout) view.findViewById(R.id.video_consultation);

        rl_viewpager = (RelativeLayout) view.findViewById(R.id.rl_upcoming_activity);

        viewpagerHealthActivity = (ViewPager) view.findViewById(R.id.viewpager_upcoming);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        viewDotIndicator = (ViewDot) view.findViewById(R.id.indicator_notifs);

        TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
        tv_name.setText("HELLO, " + MyPrefs.getString(mActivity, Constants.PREF_FIRST_NAME, ""));


        tv_label_welcome = (TextView) view.findViewById(R.id.tv_label_welcome);
        rl_create_treatment.setOnClickListener(this);
        rl_appointments.setOnClickListener(this);
        rl_reports.setOnClickListener(this);
        rl_chat.setOnClickListener(this);
        rl_chat.setOnClickListener(this);
        rl_body_vitals.setOnClickListener(this);
        rl_doctor_profile.setOnClickListener(this);

        if (Build.VERSION.SDK_INT >= 21) {
            rl_appointments.setBackground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
            rl_create_treatment.setBackground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
            rl_body_vitals.setBackground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
            rl_reports.setBackground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
            rl_chat.setBackground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
            rl_doctor_profile.setBackground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
        }


        DoctorsDepartmentsDelegate.getInstance(APP_NAME, VERSION_CODE).linkWithSingleDoctor(AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER);

        return view;
    }

    void getFirstAppointmentAndTreatment(){
        progressBar.setVisibility(View.VISIBLE);
        UpcomingActivitiesFetcher activitiesFetcher = new UpcomingActivitiesFetcher();
        activitiesFetcher.getFirstAppointmentActivityAndTreatment(getContext(),new ResponseCallback(){
            @Override
            public void onApiCallSuccess(Object responseObject) {
                ArrayList<HealthActivity> activities = (ArrayList<HealthActivity>) responseObject;
                if (activities.size() > 0) {
                    ViewPagerHealthActivity viewPagerHealthActivityAdapter = new ViewPagerHealthActivity(mActivity, activities);
                    viewpagerHealthActivity.setAdapter(viewPagerHealthActivityAdapter);
                    viewDotIndicator.setViewPager(viewpagerHealthActivity);
                    animateRL();
                }
                progressBar.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onAuthTokenRefreshRequired() {
                progressBar.setVisibility(View.INVISIBLE);
                Utility.logoutTasks(mActivity);
                Toasty.error(getContext(),"Auth token expired. please login again",Toast.LENGTH_LONG,true).show();
                Intent intent = new Intent(mActivity, SplashActivity.class);
                startActivity(intent);
                mActivity.finish();
            }

            @Override
            public void onApiCallFailure(String errorReason) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    void animateRL(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AlphaAnimation alphaAnimation = new AlphaAnimation(0,1);
//                TranslateAnimation translateAnimation = new TranslateAnimation(0,0,-200,0);
                alphaAnimation.setDuration(1000);
                alphaAnimation.setFillAfter(true);
                rl_viewpager.setAnimation(alphaAnimation);
                rl_viewpager.setVisibility(View.VISIBLE);
                alphaAnimation.start();

            }
        },500);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.rl_treatment:

                Intent intentMedicalRecords = new Intent(getContext(), TreatmentsVaccinationsClass.class);
                startActivity(intentMedicalRecords);
                break;

            case R.id.rl_upcoming_activities:

                Intent intentUpcoming = new Intent(getContext(), UpcomingActivity.class);
                startActivity(intentUpcoming);
                break;

            case R.id.rl_chat:

                Intent intentChat = new Intent(mActivity, MessageOptionsFragment.class);
                intentChat.putExtra("appName",APP_NAME);
                intentChat.putExtra("versionCode",VERSION_CODE);
                startActivity(intentChat);

                break;

            case R.id.rl_my_profile:
                BodyVitalDelegate.getInstance(APP_NAME, VERSION_CODE).startBodyVitalModule(mActivity);

                break;


            case R.id.video_consultation:
                Intent intent = new Intent(mActivity, com.healthapp.drgsgupta.AboutPartner.class);
                startActivity(intent);

                break;


            case R.id.rl_appointments:
                launchAppointmentsAndVisits();

                break;


        }
    }


    private void launchAppointmentsAndVisits() {
        AppointmentDelegate.getInstance(AppConstants.APP_NAME, AppConstants.VERSION_CODE)
                .showUserAppointmentsList(mActivity, new AppointmentsListCallback() {
                    @Override
                    public void showDepartmentListing() {
                        /**
                         * The app may have departments, or just doctors, handle that in
                         * Doctor Department module
                         * If app belongs to only one doctor only, show slots directly
                         **/

                        AppointmentDelegate.getInstance(AppConstants.APP_NAME,
                                AppConstants.VERSION_CODE)
                                .initiateBookingForDoctor(getContext(),
                                        AppConstants.MEDICAL_DIRECTOR_NAME, AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER, "", "","");
                    }

                    @Override
                    public void startHomeActivity(Context context) {
                        Intent intent = new Intent(context, HomeActivity.class);
                        context.startActivity(intent);
                    }
                });
    }


    @Override
    public void onResume() {
        super.onResume();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getFirstAppointmentAndTreatment();

            }
        }, 500);

    }

    @Override
    public void onStop() {
        super.onStop();
        if(disposable!=null){
            disposable.dispose();
        }
    }
}