package com.healthapp.drgsgupta;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.navia.modules.reportmodule.ReportDelegate;
import com.navia.modules.treatmentmodule.TreatmentModuleDelegate;
import com.navia.naviahelp.HelperFunctions;

/**
 * Created by JaiGuruji on 23/02/17.
 */

public class TreatmentsVaccinationsClass extends AppCompatActivity implements View.OnClickListener {

    CardView card_treatments,card_vaccine,card_past_treatments,card_reports;
    TextView tv_label_treatments,tv_label_vaccinations,tv_label_past_treatments,tv_label_reports;
    TextView tv_label_treatments_desc,tv_label_vaccinations_desc,tv_label_past_treatments_desc;
    Activity mActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_treatments);
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        toolbar.setTitle("Treatments");
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        HelperFunctions.changeToolbarTitle(toolbar,FontNamesClass.fontSemibold);
        mActivity = this;
        card_treatments = (CardView)findViewById(R.id.card_treatments);
        card_treatments.setOnClickListener(this);
        card_past_treatments = (CardView)findViewById(R.id.card_past_treatments);
        card_past_treatments.setOnClickListener(this);
        card_vaccine = (CardView)findViewById(R.id.card_vaccination);
        card_reports = (CardView)findViewById(R.id.card_reports);
        card_vaccine.setOnClickListener(this);
        card_reports.setOnClickListener(this);

        Typeface typeface_medium = FontCache.getTypeFace(FontNamesClass.fontSemibold,this);
        Typeface typeface_regular = FontCache.getTypeFace(FontNamesClass.fontSemibold,this);



        tv_label_treatments= (TextView)findViewById(R.id.tv_label_treatments);
        tv_label_treatments.setTypeface(typeface_medium);
        tv_label_past_treatments = (TextView)findViewById(R.id.tv_label_past_treatments);
        tv_label_past_treatments.setTypeface(typeface_medium);
        tv_label_vaccinations = (TextView)findViewById(R.id.tv_label_vaccinations);
        tv_label_vaccinations.setTypeface(typeface_medium);
        tv_label_reports = (TextView)findViewById(R.id.tv_label_reports);

        tv_label_treatments_desc = (TextView)findViewById(R.id.tv_label_treatment_description);
        tv_label_vaccinations_desc = (TextView)findViewById(R.id.tv_vaccination_description);
        tv_label_past_treatments_desc = (TextView)findViewById(R.id.tv_past_treatment_description);
        tv_label_treatments_desc.setTypeface(typeface_regular);
        tv_label_vaccinations_desc.setTypeface(typeface_regular);
        tv_label_past_treatments_desc.setTypeface(typeface_regular);
        tv_label_reports.setTypeface(typeface_medium);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.card_treatments :
                TreatmentModuleDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).startTreatmentModule(mActivity,TreatmentModuleDelegate.DOCTOR_PHONE);
                break;
            case R.id.card_past_treatments:
                TreatmentModuleDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).startPastTreatment(mActivity);
                break;
            case R.id.card_vaccination:
                Intent intent2 = new Intent(mActivity,VaccinationSchedulesClass.class);
                startActivity(intent2);
                break;
            case R.id.card_reports:
                ReportDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).startReportModule(mActivity);
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return true;
    }

}
