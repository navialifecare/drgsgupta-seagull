package com.healthapp.drgsgupta;

/**
 * Created by JaiGuruji on 26/12/16.
 */

public class DoctorItem {
    String mDoctorName;
    String mLast_chatted_on;
    String mDoctorNumber;
    String mDoctorSpecialization;

    public DoctorItem() {
    }

    public String getmDoctorName() {
        return mDoctorName;
    }

    public void setmDoctorName(String mDoctorName) {
        this.mDoctorName = mDoctorName;
    }

    public String getmDoctorNumber() {
        return mDoctorNumber;
    }

    public void setmDoctorNumber(String mDoctorNumber) {
        this.mDoctorNumber = mDoctorNumber;
    }

    public String getmLast_chatted_on() {
        return mLast_chatted_on;
    }

    public void setmLast_chatted_on(String mLast_chatted_on) {
        this.mLast_chatted_on = mLast_chatted_on;
    }

    public String getmDoctorSpecialization() {
        return mDoctorSpecialization;
    }

    public void setmDoctorSpecialization(String mDoctorSpecialization) {
        this.mDoctorSpecialization = mDoctorSpecialization;
    }
}
