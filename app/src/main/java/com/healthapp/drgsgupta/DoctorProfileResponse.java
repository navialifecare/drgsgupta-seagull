package com.healthapp.drgsgupta;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by amitgupta on 3/26/18.
 */

public class DoctorProfileResponse {

    @SerializedName("result")
    String  result;

    @SerializedName("data")
    DoctorProfileData doctorProfileData;


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public DoctorProfileData getDoctorProfileData() {
        return doctorProfileData;
    }

    public void setDoctorProfileData(DoctorProfileData doctorProfileData) {
        this.doctorProfileData = doctorProfileData;
    }

    class DoctorProfileData implements Parcelable{
        String specialization;
        @SerializedName("name")
        String doctorName;
        @SerializedName("from_type")
        String hospital;
        String state;
        @SerializedName("clinic_name")
        String clinicName;
        String city;
        @SerializedName("address_line")
        String address;
        @SerializedName("timefrom")
        String timeFrom;
        String biography;
        String email;
        @SerializedName("timeto")
        String timeTo;
        @SerializedName("workingdays")
        String workingDays;
        @SerializedName("pin_code")
        String pinCode;
        String image;


        public DoctorProfileData() {
        }

        public  DoctorProfileData(Parcel in){
            this.address=in.readString();
            this.doctorName=in.readString();
            this.hospital=in.readString();
            this.state=in.readString();
            this.address=in.readString();
            this.clinicName=in.readString();
            this.city=in.readString();
            this.address=in.readString();
            this.timeFrom=in.readString();
            this.biography=in.readString();
            this.email=in.readString();

        }



        public String getSpecialization() {
            return specialization;
        }

        public void setSpecialization(String specialization) {
            this.specialization = specialization;
        }

        public String getDoctorName() {
            return doctorName;
        }

        public void setDoctorName(String doctorName) {
            this.doctorName = doctorName;
        }


        public String getHospital() {
            return hospital;
        }

        public void setHospital(String hospital) {
            this.hospital = hospital;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getClinicName() {
            return clinicName;
        }

        public void setClinicName(String clinicName) {
            this.clinicName = clinicName;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getTimeFrom() {
            return timeFrom;
        }

        public void setTimeFrom(String timeFrom) {
            this.timeFrom = timeFrom;
        }

        public String getBiography() {
            return biography;
        }

        public void setBiography(String biography) {
            this.biography = biography;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getTimeTo() {
            return timeTo;
        }

        public void setTimeTo(String timeTo) {
            this.timeTo = timeTo;
        }

        public  final Creator CREATOR = new Creator<DoctorProfileData>(){
            @Override
            public DoctorProfileData createFromParcel(Parcel source) {
                return new DoctorProfileData(source);
            }

            @Override
            public DoctorProfileData[] newArray(int size) {
                return new DoctorProfileData[0];
            }
        };

        public String getWorkingDays() {
            return workingDays;
        }

        public void setWorkingDays(String workingDays) {
            this.workingDays = workingDays;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {

        }

        public String getPinCode() {
            return pinCode;
        }

        public void setPinCode(String pinCode) {
            this.pinCode = pinCode;
        }


        public String getImage() {
            return image;
        }
    }

}
