package com.healthapp.drgsgupta;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

/**
 * Created by JaiGuruji on 16/05/16.
 */
public class GCMIntentService extends IntentService {

    public static final String REGISTRATION_SUCCESS = "RegistrationSuccess";
    public static final String REGISTRATION_ERROR = "RegistrationError";


    public GCMIntentService() {
        super("");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        registerGCM();
    }

    public void registerGCM() {
        Intent registrationComplete = null;
        String token = null;
        try {
            InstanceID id = InstanceID.getInstance(getApplicationContext());
            token = id.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.e("GCMIntentService", token);
            System.out.print(token);
            registrationComplete = new Intent(REGISTRATION_SUCCESS);
            registrationComplete.putExtra("token", token);
        } catch (Exception e) {
            Log.v("GCMIntentService", "Registration Error");
            registrationComplete = new Intent(REGISTRATION_ERROR);
        }
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.sendBroadcast(registrationComplete);
    }
}
