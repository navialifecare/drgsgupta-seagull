package com.healthapp.drgsgupta;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;

import com.healthapp.appointment_module.AppointmentDelegate;
import com.healthapp.appointment_module.AppointmentsListCallback;
import com.healthapp.body_vital_module.BodyVitalDelegate;
import com.healthapp.broadcast_module.BroadcastDelegate;
import com.healthapp.chat_module.XmppChatDelegate;
import com.healthapp.usermodule.UserDelegate;
import com.healthapp.usermodule.UserProfileCallback;
import com.navia.modules.reportmodule.ReportDelegate;
import com.navia.modules.treatmentmodule.TreatmentModuleDelegate;
import com.navia.modules.treatmentmodule.data.database.TasksDataSource;
import com.healthapp.drgsgupta.health_activity.ui.UpcomingActivity;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.MyPrefs;

import static com.healthapp.drgsgupta.AppConstants.APP_NAME;
import static com.healthapp.drgsgupta.AppConstants.VERSION_CODE;

/**
 * Created by amitgupta on 12/20/17.
 */

public class UserProfileFragment extends Fragment implements View.OnClickListener{
    Activity mActivity;
    ImageView img_logout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);

        mActivity = getActivity();

        CardView card_profile = (CardView)view.findViewById(R.id.card_profile);
        CardView card_treatments = (CardView)view.findViewById(R.id.card_treatments);
        CardView card_appointments = (CardView)view.findViewById(R.id.card_appointments);
        CardView card_broadcast = (CardView)view.findViewById(R.id.card_broadcast);
        CardView card_reports = (CardView)view.findViewById(R.id.card_reports);
        CardView card_chats = (CardView)view.findViewById(R.id.card_chats);
        CardView card_upcoming_activity = (CardView) view.findViewById(R.id.card_upcoming_activity);

        TextView tv_name = (TextView)view.findViewById(R.id.tv_name);
        TextView tv_phone_no = (TextView)view.findViewById(R.id.tv_phone);
        TextView tv_name_initial = (TextView)view.findViewById(R.id.tv_name_initial);

        String name =MyPrefs.getString(mActivity, Constants.PREF_FIRST_NAME,"");
        tv_name.setText(MyPrefs.getString(mActivity, Constants.PREF_FIRST_NAME,""));
        tv_phone_no.setText(MyPrefs.getString(mActivity,Constants.PHONE_ID,""));
        try {
            tv_name_initial.setText(name.charAt(0) + "");
        }catch (StringIndexOutOfBoundsException e){
            e.printStackTrace();
        }

        img_logout = (ImageView)view.findViewById(R.id.img_settings);

        card_appointments.setOnClickListener(this);
        card_profile.setOnClickListener(this);

        card_treatments.setOnClickListener(this);
        card_broadcast.setOnClickListener(this);
        card_reports.setOnClickListener(this);
        card_chats.setOnClickListener(this);
        card_upcoming_activity.setOnClickListener(this);
        img_logout.setOnClickListener(this);

        if(Build.VERSION.SDK_INT>=21){
            card_appointments.setForeground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
            card_treatments.setForeground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
            card_broadcast.setForeground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
            card_reports.setForeground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
            card_profile.setForeground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
            card_upcoming_activity.setForeground(getResources().getDrawable(R.drawable.bg_ripple_dark_green));
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        Intent intent=null;
        switch (v.getId()){
            case R.id.card_profile :
                UserDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE,AppConstants.MEDICAL_DIRECTOR_NAME).launchUserProfile(mActivity, new UserProfileCallback() {
                    @Override
                    public void openBodyVitals(Activity activity) {
                        BodyVitalDelegate.getInstance(APP_NAME,VERSION_CODE).startBodyVitalModule(mActivity);
                    }

                    @Override
                    public void startSplashActivityAndFinishAffinity() {
                        if(mActivity!=null) {
                            Intent i = new Intent(mActivity, SplashActivity.class);
                            startActivity(i);
                            mActivity.finish();
                        }
                    }
                });
                break;
            case R.id.card_treatments :
              TreatmentModuleDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).startTreatmentModule(mActivity,TreatmentModuleDelegate.DOCTOR_PHONE);

                break;
            case R.id.card_appointments :
                launchAppointmentsAndVisits();

                break;
            case R.id.card_broadcast :
                BroadcastDelegate.getInstance(APP_NAME, VERSION_CODE).startBroadcastModule(mActivity);

                break;
            case R.id.card_reports :
                ReportDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).startReportModule(mActivity);

                break;
            case R.id.card_chats:
                XmppChatDelegate.getInstance(AppConstants.APP_NAME, VERSION_CODE).openChatWithDoctor(AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER,
                        AppConstants.MEDICAL_DIRECTOR_NAME, mActivity);

                break;
            case R.id.card_upcoming_activity:
                Intent intentUpcoming = new Intent(getContext(), UpcomingActivity.class);
                startActivity(intentUpcoming);
                break;
            case R.id.img_settings:
                AlertDialog dialog=  new AlertDialog.Builder(getContext())
                        .setTitle("Warning")
                        .setMessage(getString(R.string.message_log_out))
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                logOutTasks();
                                Intent intentlogin = new Intent(getActivity(),SplashActivity.class);
                                intentlogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intentlogin);
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton(android.R.string.no,null).create();
                dialog.getWindow().getAttributes().windowAnimations= R.style.DialogTheme;
                dialog.show();
        }
    }


    private void launchAppointmentsAndVisits() {
        AppointmentDelegate.getInstance(AppConstants.APP_NAME, AppConstants.VERSION_CODE)
                .showUserAppointmentsList(mActivity, new AppointmentsListCallback() {
                    @Override
                    public void showDepartmentListing() {
                        /**
                         * The app may have departments, or just doctors, handle that in
                         * Doctor Department module
                         * If app belongs to only one doctor only, show slots directly
                         **/

                        AppointmentDelegate.getInstance(AppConstants.APP_NAME,
                                AppConstants.VERSION_CODE)
                                .initiateBookingForDoctor(getContext(),
                                        AppConstants.MEDICAL_DIRECTOR_NAME, AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER, "", "","");
                    }

                    @Override
                    public void startHomeActivity(Context context) {
                        Intent  intent=new Intent(context, HomeActivity.class);
                        context.startActivity(intent);
                    }
                });

    }

    void logOutTasks(){
        ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Deleting all data for user");
        progressDialog.show();
        // Perform logout tasks
        MyPrefs.clear(getContext());
        MyPrefs.putBoolean(getContext(),Constants.OPENED_ONCE,true);
        int currentVersionCode = 0;
        try {
            currentVersionCode = getActivity().getPackageManager().
                    getPackageInfo(getActivity().getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            // handle exception
            e.printStackTrace();
            return;
        }
        MyPrefs.putInt(getContext(),Constants.VERSION_CODE_KEY,currentVersionCode);
        deleteTreatment();

        XmppChatDelegate.getInstance(AppConstants.APP_NAME, AppConstants.VERSION_CODE)
                .stopXmppChat(getContext());
        // Dismiss the progress dialog
        progressDialog.dismiss();
    }

    private void deleteTreatment() {
        TasksDataSource db = TasksDataSource.getInstance(getActivity()); //get access to the instance of TasksDataSource
        // Delete all treatments
         db.deleteAllTasks();
        //Delete all follow ups
         db.deleteAllFolUpTasks();
        //Delete all side effects
         db.deleteAllSideEffects();
         db.deleteAllParameters();
    }


}
