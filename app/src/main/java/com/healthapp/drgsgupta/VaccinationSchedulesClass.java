package com.healthapp.drgsgupta;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.healthapp.drgsgupta.Adapter.VaccinationListAdapter;
import com.healthapp.drgsgupta.task.VaccinationItem;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.HelperFunctions;
import com.navia.naviahelp.MyPrefs;
import com.navia.naviahelp.OkhttpClientManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by JaiGuruji on 22/02/17.
 */

public class VaccinationSchedulesClass extends AppCompatActivity implements AsyntaskCompleteInterface {

    RecyclerView mRecyclerView;
    ArrayList<VaccinationItem> mArrayList;
    TextView tv_empty_msg,tv_label_vaccines;
    ProgressBar progressBar;
    Activity mContext;
    Disposable disposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaccination);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        toolbar.setTitle("Vaccines");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = this;

        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        mArrayList= new ArrayList<>();

        tv_empty_msg = (TextView)findViewById(R.id.img_empty);
        tv_label_vaccines = (TextView)findViewById(R.id.tv_vaccine_label);

        fetchSchedulesFromBackEnd();

    }
//
//    private void fetchSchedulesFromBackend(){
//        String URL = Constants.URL.replace("/navia/","/messaging/get_vaccination_list/");
//        JSONObject jsonObject = new JSONObject();
//        try{
//          jsonObject.put("username", MyPrefs.getString(this,Constants.PHONE_ID,""));
//        }catch (Exception e){
//            e.printStackTrace();
//        }
////        FetchVaccineSchedulesAsyncTask asyncTask = new FetchVaccineSchedulesAsyncTask(this,URL,jsonObject.toString(),this);
////        asyncTask.execute();
//    }


    private  void fetchSchedulesFromBackEnd(){

        final ArrayList<VaccinationItem> arrayListVaccines = new ArrayList<>();

            progressBar.setVisibility(View.VISIBLE);

            final String URL = Constants.URL.replace("/navia/","/messaging/get_vaccination_list/");
            final JSONObject jsonObject = new JSONObject();
            try{
                jsonObject.put("username", MyPrefs.getString(this,Constants.PHONE_ID,""));
            }catch (Exception e){
                e.printStackTrace();
            }


        Observable.defer(new Callable<ObservableSource<String>>() {
            @Override
            public ObservableSource<String> call() throws Exception {
                return Observable.just(OkhttpClientManager.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).postRequest(URL,jsonObject.toString()));
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable=d;
                    }

                    @Override
                    public void onNext(String s){
                        try {
                            JSONObject response =new JSONObject(s);
                            if (response.getString("result").equals("SUCCESS")) {
                                progressBar.setVisibility(View.INVISIBLE);
                                JSONArray jsonArray = response.getJSONArray("list");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject vaccine_list_item = jsonArray.getJSONObject(i);
                                    VaccinationItem item = new VaccinationItem();
                                    item.setmBabyName(vaccine_list_item.getString("name"));
                                    item.setmStartDate(vaccine_list_item.getString("start_date"));
                                    item.setmDoctorName(vaccine_list_item.getString("doctor"));
                                    item.setJsonArray(new JSONArray(vaccine_list_item.getString("schedule")));
                                    arrayListVaccines.add(item);
                                }
                                if(arrayListVaccines.size()>0) {
                                    VaccinationListAdapter vaccinationListAdapter = new VaccinationListAdapter(arrayListVaccines, mContext, mRecyclerView);
                                    mRecyclerView.setAdapter(vaccinationListAdapter);
                                }
                                else {
                                    tv_empty_msg.setVisibility(View.VISIBLE);
                                    tv_label_vaccines.setVisibility(View.INVISIBLE);
                                }

                            } else if (response.getString("result").equals("FAILURE")) {
                                progressBar.setVisibility(View.INVISIBLE);
                                HelperFunctions.displayToast(mContext, response.getString("error_reason"));
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                            progressBar.setVisibility(View.INVISIBLE);
                            tv_empty_msg.setVisibility(View.VISIBLE);
                            tv_label_vaccines.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onError(Throwable e) { }

                    @Override
                    public void onComplete() { }
                });

    }

    @Override
    public void asyncTaskCompleted(ArrayList arrayList) {
        mArrayList = arrayList;
        if(arrayList.size()>0) {
            VaccinationListAdapter vaccinationListAdapter = new VaccinationListAdapter(arrayList, this, mRecyclerView);
            mRecyclerView.setAdapter(vaccinationListAdapter);
        }
        else {
            tv_empty_msg.setVisibility(View.VISIBLE);
            tv_label_vaccines.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        if(disposable!=null){
            disposable.dispose();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return true;
    }
}
