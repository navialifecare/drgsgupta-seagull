package com.healthapp.drgsgupta;

/**
 * Created by amitgupta on 9/18/17.
 */

public class FontNamesClass {
    public static final String fontRegular="SourceSansPro_Regular.otf";
    public static final String fontBold="SourceSansPro_Bold.otf";
    public static final String fontSemibold="SourceSansPro_Semibold.otf";
}
