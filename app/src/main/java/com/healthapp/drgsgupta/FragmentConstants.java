package com.healthapp.drgsgupta;

/**
 * Created by amitgupta on 1/2/18.
 */

public class FragmentConstants {

    public static final String HOMEFRAGMENT="HOME_FRAGMENT";
    public static final String CONTENTFRAGMENT="CONTENT_FRAGMENT";
    public static final String USERPROFILEFRAGMENT="USERPROFILE_FRAGMENT";
}
