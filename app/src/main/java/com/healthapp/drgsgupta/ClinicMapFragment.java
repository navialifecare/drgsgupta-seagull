package com.healthapp.drgsgupta;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.healthapp.drgsgupta.R;

/**
 * Created by JaiGuruji on 12/09/16.
 */
public class ClinicMapFragment extends Fragment {
    MapFragment fragment;
    double mLattitude;
    double mLongitude;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.clinic_map_layout, container, false);
        } catch (InflateException e) {
            /* map is already there, just return view as it is */
        }
        fragment = (MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.clinic_map);
        mLattitude = getArguments().getDouble("latitude");
        mLongitude = getArguments().getDouble("longitude");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Clinic Location");
        fragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                MarkerOptions options = new MarkerOptions().position(new LatLng(mLattitude, mLongitude))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)).
                                title("Diabetes, Hypertension & Heart Clinic");
                googleMap.addMarker(options);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mLattitude, mLongitude), 17f);
                googleMap.animateCamera(cameraUpdate);
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        try {
            super.onDestroyView();
            getActivity().finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
