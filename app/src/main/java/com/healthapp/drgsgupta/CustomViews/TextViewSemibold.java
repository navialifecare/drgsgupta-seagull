package com.healthapp.drgsgupta.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.healthapp.drgsgupta.FontCache;
import com.healthapp.drgsgupta.FontNamesClass;


/**
 * Created by amitgupta on 10/26/17.
 */

public class TextViewSemibold extends TextView {

    public TextViewSemibold(Context context) {
        super(context);
        applySemiBoldFont(context);
    }

    public TextViewSemibold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applySemiBoldFont(context);
    }

    public TextViewSemibold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applySemiBoldFont(context);
    }

    void applySemiBoldFont(Context context){
        Typeface typeface = FontCache.getTypeFace(FontNamesClass.fontSemibold,context);
        setTypeface(typeface);
    }


}
