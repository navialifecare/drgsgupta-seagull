package com.healthapp.drgsgupta.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.healthapp.drgsgupta.FontNamesClass;

/**
 * Created by amitgupta on 11/23/17.
 */

public class TextViewRegular extends TextView{
    Context mContext;

    public TextViewRegular(Context context) {
        super(context);
        mContext = context;
        setFont(context);
    }

    public TextViewRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }

    public TextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }
    void setFont(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), FontNamesClass.fontRegular);
        setTypeface(typeface);
    }
}
