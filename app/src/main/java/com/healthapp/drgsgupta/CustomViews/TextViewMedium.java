package com.healthapp.drgsgupta.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.healthapp.drgsgupta.FontNamesClass;

/**
 * Created by amitgupta on 12/15/17.
 */

public class TextViewMedium extends TextView {
    Context mContext;

    public TextViewMedium(Context context) {
        super(context);
        mContext = context;
        setFont(context);
    }

    public TextViewMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }

    public TextViewMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }
    void setFont(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), FontNamesClass.fontSemibold);
        setTypeface(typeface);
    }
}