package com.healthapp.drgsgupta;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.healthapp.appointment_module.AppointmentDelegate;
import com.healthapp.appointment_module.AppointmentFetchResponse;
import com.healthapp.appointment_module.AppointmentsListItem;
import com.healthapp.drgsgupta.health_activity.data.ActivityType;
import com.healthapp.drgsgupta.health_activity.data.HealthActivity;
import com.navia.modules.treatmentmodule.data.Task;
import com.navia.modules.treatmentmodule.data.database.TasksDataSource;
import com.navia.naviahelp.ApiConstants;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.HelperFunctions;
import com.navia.naviahelp.MyPrefs;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import de.measite.minidns.record.A;
import es.dmoral.toasty.Toasty;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by amitgupta on 5/4/18.
 */

public class UpcomingActivitiesFetcher  {

    public ArrayList<HealthActivity> getFirstAppointment(Context context) throws IOException{

        ArrayList<HealthActivity> activities = new ArrayList<>();
        AppointmentFetchResponse appointmentFetchResponse= AppointmentDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).getAppointments(MyPrefs.getString(context, Constants.PHONE_ID, ""));

        if(appointmentFetchResponse.getResult().equals(Constants.AUTH_TOKEN_REFRESH_REQUIRED)){
            Utility.logoutTasks(context);
            Toasty.error(context,"Auth token expired. please login again",Toast.LENGTH_LONG,true).show();
            Intent intent = new Intent(context, SplashActivity.class);
            ((AppCompatActivity)context).startActivity(intent);
            ((AppCompatActivity)context).finish();
            return null;
        }
        if(appointmentFetchResponse.getResult().equals(ApiConstants.API_CALL_SUCCESS)){
            try {
                List<AppointmentFetchResponse.AppointmentDataItem> appointmentDataItems = appointmentFetchResponse.getData();
                for (AppointmentFetchResponse.AppointmentDataItem appointmentDataItem : appointmentDataItems) {
                    AppointmentsListItem appointmentsListItem = appointmentDataItem.getAppointmentsListItem();
                    HealthActivity healthActivity = new HealthActivity();
                    healthActivity.setName("Appointment with Dr. " + appointmentsListItem.getDoctorName());
                    healthActivity.setTime(appointmentsListItem.getBookingTime());
                    healthActivity.setDate(appointmentDataItem.getAppointmentsListItem().getBookingDate());
                    String datetime = appointmentDataItem.getAppointmentsListItem().getBookingDate() + " " +
                            appointmentDataItem.getAppointmentsListItem().getBookingTime();
                    healthActivity.setType(ActivityType.APPOINTMENT);
                    if (HelperFunctions.convertDateStringTolong(datetime, 11) > System.currentTimeMillis() && appointmentsListItem.getDoctorName().toLowerCase().contains("gupta")) {
                        activities.add(healthActivity);
                        return activities;
                    }
                }
            }
            catch(Exception e){
                return  activities;

            }
        }


        return  activities;
    }


    public ArrayList<HealthActivity> getFirstTreatmentActivity(Context context){

        ArrayList<HealthActivity> activities = new ArrayList<>();
        List<Task> tasks = TasksDataSource.getInstance(context).getAllTasks();
        for (int i = 0; i < tasks.size(); i++) {
            HealthActivity healthActivity = new HealthActivity();
            healthActivity.setName("Your upcoming pill is "+tasks.get(i).getName() + " (" + tasks.get(i).getNotes() + ")");
            healthActivity.setTimeMillis(tasks.get(i).getDateDue());
            healthActivity.setDate(HelperFunctions.convertLongToDate(healthActivity.getTimeMillis(), 3));
            healthActivity.setTime(new SimpleDateFormat("h:mm a")
                    .format(new Date(healthActivity.getTimeMillis())));
            healthActivity.setType(ActivityType.TREATMENT);
            if (healthActivity.getTimeMillis() > System.currentTimeMillis()) {
                ArrayList<HealthActivity> arrayList = new ArrayList();
                arrayList.add(healthActivity);
                return arrayList;
            }

        }
        return activities;
    }


    public ArrayList<HealthActivity> getFirstAppointmentActivityAndTreatment(final Context context, final ResponseCallback apiCallback){
        Observable observableAppointments=Observable.defer(new Callable<ObservableSource<ArrayList<HealthActivity>>>() {
            @Override
            public ObservableSource<ArrayList<HealthActivity>> call() throws Exception {
                return Observable.just(getFirstAppointment(context));
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());



        Observable observableTreatments = Observable.defer(new Callable<ObservableSource<ArrayList<HealthActivity>>>() {
            @Override
            public ObservableSource<ArrayList<HealthActivity>> call() throws Exception {
                return Observable.just(getFirstTreatmentActivity(context));
            }
        }).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread());


        final ArrayList<HealthActivity> activities = new ArrayList<>();
        Observable.zip(observableAppointments, observableTreatments, new BiFunction() {
            @Override
            public Object apply(Object arrayListTreatment, Object arrayListAppointment) throws Exception {
                ArrayList<HealthActivity> arrayList = new ArrayList();
                if(((ArrayList<HealthActivity>)arrayListAppointment).size()>0) {
                    arrayList.addAll((ArrayList<HealthActivity> )arrayListAppointment);
                }
                if(((ArrayList<HealthActivity>)arrayListTreatment).size()>0) {
                    arrayList.addAll((ArrayList<HealthActivity>) arrayListTreatment);
                }
                return arrayList;
            }
        }).subscribe(new Observer() {
            @Override
            public void onSubscribe(Disposable d) { }

            @Override
            public void onNext(Object o) {
                activities.addAll((ArrayList<HealthActivity>)o);
                apiCallback.onApiCallSuccess(activities);
            }

            @Override
            public void onError(Throwable e) {
                Log.v("error",e.getMessage());
            }

            @Override
            public void onComplete() { }
        });


        return activities;
    }
}
