package com.healthapp.drgsgupta;

import android.content.Context;
import android.support.multidex.BuildConfig;
import android.support.v4.app.Fragment;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by lenovo on 9/25/2015.
 */
public class MyApplication extends com.navia.naviahelp.MyApplication {
    public static Context context;
    private static Fragment fragment;
    private static String stateoffragment;
    // remove OTP_DEBUG when release this app with Pharmacy Screens
    public static boolean hasPharmacy = BuildConfig.DEBUG;

    public static String getStateoffragment() {
        return stateoffragment;
    }

    public static void setStateoffragment(String stateoffragment1) {
        stateoffragment = stateoffragment1;
    }

    public static void setContext(Context context) {
        MyApplication.context = context;
    }

    public static Fragment getFragment() {
        return fragment;
    }

    public static void setFragment(Fragment fragment) {
        fragment = fragment;
    }


    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        Crashlytics crashlytics = new Crashlytics();
//        if (crashlytics != null)
//            Fabric.with(this, crashlytics);
        Fabric.with(this, new Crashlytics());
        context = this;

        // Normal app init code...


        // TODO: 22/07/16 Remove Stetho before publishing
        //Stetho.initializeWithDefaults(this);
    }
}
