package com.healthapp.drgsgupta;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Hashtable;

/**
 * Created by JaiGuruji on 20/04/17.
 */

public class FontCache {

    private static Hashtable<String,Typeface> hashtable = new Hashtable<>();

    public static Typeface getTypeFace(String name, Context context){
        Typeface tp = hashtable.get(name);

        if(tp==null) {
            try {
                tp = Typeface.createFromAsset(context.getAssets(), name);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        hashtable.put(name,tp);
        return tp;
    }
}
