package com.healthapp.drgsgupta.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.healthapp.appointment_module.Visit;
import com.healthapp.appointment_module.VisitFeedbackDelegate;
import com.healthapp.drgsgupta.AppConstants;
import com.healthapp.drgsgupta.HomeActivity;
import com.healthapp.drgsgupta.MyApplication;
import com.healthapp.drgsgupta.NaviaContentActivity.ContentActivity;
import com.healthapp.drgsgupta.R;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.MyPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;


/**
 * Created by amitgupta on 11/14/17.
 */

public class FirebaseMessageReceivingService extends FirebaseMessagingService {
    Context mContext;
    String channelId = "200";
    String channelName = "FirebaseNotificationChannel";
    String requestCode = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        mContext = this;

        //Return if user is logged out.
        if (!MyPrefs.getBoolean(mContext, Constants.PREF_REGISTERED, false)) {
            return;
        }

        Map<String, String> hashmap = remoteMessage.getData();

//        String title = remoteMessage.getNotification().getTitle();
//        String body = remoteMessage.getNotification().getBody();
//        String message_code = hashmap.get("message_code");

        processInfo(hashmap);
    }

    private void processInfo(Map<String, String> hashMap) {
        try {
            String message_code = hashMap.get("message_code");
            String doctor_name, doctor_number, messageTitle = "";

            switch (message_code) {
                case "100":
                    String text = hashMap.get("message");
                    break;

                case "200": //New Treatment

                    doctor_name = hashMap.get("doctor_name");
                    doctor_number = hashMap.get("doctor_number");
                    JSONObject treatment_details = new JSONObject(hashMap.get("data"));
                    //  addTreatment(true,treatment_details,doctor_name,doctor_number);
                    sendNotification(true, "New Treatment added by " + doctor_name,
                            doctor_name, "");
                    break;

                case "300":  //New FollowUp
                    if (MyPrefs.getBoolean(MyApplication.getContext(), Constants.PREF_REGISTERED, false)) {
                        doctor_name = hashMap.get("doctor_name");
                        doctor_number = hashMap.get("doctor_number");
                        JSONObject followupDetails = new JSONObject(hashMap.get("data"));
                        messageTitle = "New follow-up created, please check the app to view follow up";
                        sendNotification(true, messageTitle, doctor_name, "");
                    }
                    break;

                case "700": //New Content article

                    String title = hashMap.get("title");
                    String img_url = hashMap.get("image_url");
                    String id = hashMap.get("content_id");
                    String date = hashMap.get("creation_date");
                    receiveImageNotification(id, title, img_url, id, date);
                    break;

                case "1000": //Message from Navia
                    if (MyPrefs.getBoolean(MyApplication.getContext(), Constants.PREF_REGISTERED, false)) {
                        messageTitle = hashMap.get("message");
                        if (hashMap.get("doctor_name") == null)
                            doctor_name = getString(R.string.doctor_name);
                        else
                            doctor_name = hashMap.get("doctor_name");
                        requestCode = "1000";
                        sendNotification(false, messageTitle, doctor_name, doctor_name);
                    }
                    break;

                case "2000": //Visit completed
                    if (MyPrefs.getBoolean(MyApplication.getContext(), Constants.PREF_REGISTERED, false)) {
                        Log.d("test_firebase", "processInfo: " + hashMap.toString());
                        //update the new visit in DB
                        VisitFeedbackDelegate visitFeedbackDelegate = VisitFeedbackDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE);
                        Visit visit = new Visit();
                        visit.setPk(hashMap.get("visit_id"));
                        visit.setDate(hashMap.get("visit_date"));
                        visit.setTime(hashMap.get("visit_time"));
                        visit.setDoctor_name(hashMap.get("doctor_name"));
                        visit.setRecommendationMessage(hashMap.get("recommendation_message"));
                        visitFeedbackDelegate.storeNewVisit(visit);

                        //send notification
                        String message = hashMap.get("message");

                        sendNotification(false, message,
                                visit.getDoctor_name(), visit.getDoctor_name());
                    }
                    break;
            }
        } catch (JSONException e) {

        }

    }


    private void sendNotification(Boolean CurrentTreatments, String message, String doctorname, String doctorName) {

        Intent intent = new Intent(this, HomeActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if (message.equals("")) {
            message = "New message from " + AppConstants.HOSPITAL_NAME;
        }

        Random random = new Random();
        int mNotifId = random.nextInt(999999 - 1111 + 1) + 1111;
        int requestcode = 0;

        PendingIntent intent1 = PendingIntent.getActivity(this, requestcode, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.app_icon);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext()).setContentTitle("New Message From Doctor " + doctorname).
                setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setLargeIcon(icon)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.app_icon).setSound(sound).setContentText("My First GCM Push Notification").setContentText(message).
                        setContentIntent(intent1);


        builder.setContentTitle("New Message From " + AppConstants.HOSPITAL_NAME);
        setNotificationDefaults(mNotifId, builder);
    }

    void receiveImageNotification(String content, String title, String image_url, String id, String date) {
        Intent intent = new Intent(this, ContentActivity.class);
        intent.putExtra("title_article", title);
        intent.putExtra("img_article", image_url);
        intent.putExtra("content_article", content);
        intent.putExtra("id_article", id);
        intent.putExtra("date_article", date);


        Random random = new Random();
        int mNotifId = random.nextInt(999999 - 1111 + 1) + 1111;

        PendingIntent pendingIntent = PendingIntent.getActivity(this, mNotifId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.app_icon);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
        builder.setTicker("New content from " + AppConstants.HOSPITAL_NAME);
        builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(getBitmapFromURL(image_url)));
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.drawable.app_icon).setLargeIcon(icon).setSound(sound);
        setNotificationDefaults(mNotifId, builder);
    }


    private Bitmap getBitmapFromURL(String url) {
        try {
            URL url_conn = new URL(url);
            HttpsURLConnection httpURLConnection = (HttpsURLConnection) url_conn.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            return bitmap;


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    void setNotificationDefaults(int notif_id, NotificationCompat.Builder builder) {

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = builder.setChannelId(channelId).build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.ledARGB = 0xffff0000;
        notification.ledOnMS = 300;
        notification.ledOffMS = 1400;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
            manager.createNotificationChannel(channel);
        }

        manager.notify(notif_id, notification);
    }


}
