package com.healthapp.drgsgupta.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.navia.modules.treatmentmodule.TreatmentModuleDelegate;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.MyPrefs;
import com.navia.naviahelp.OkhttpClientManager;

import org.json.JSONObject;

/**
 * Created by JaiGuruji on 15/02/17.
 */

public class VaccinationStatusService extends IntentService {


    public VaccinationStatusService(String name) {
        super(name);
    }
    public VaccinationStatusService() {
        super("VaccinationStatusService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String URL = Constants.URL.replace("/navia/","/messaging/");
        URL = URL+"vaccine/edit_vaccination";
        JSONObject jsonObject = new JSONObject();
        int notif_id =intent.getIntExtra("notification_id",0);
        try{
            Toast.makeText(this,""+intent.getStringExtra("is_taken"),Toast.LENGTH_LONG).show();
            if(intent.getStringExtra("is_taken").equals("yes")) {
                jsonObject.put("username", MyPrefs.getString(this, Constants.PHONE_ID, ""));
                jsonObject.put("child_name", intent.getStringExtra("child_name"));
                jsonObject.put("vaccine", intent.getStringExtra("vaccine"));
                jsonObject.put("complete", "true");

                OkhttpClientManager.getInstance(TreatmentModuleDelegate.APP_NAME,TreatmentModuleDelegate.VERSION_CODE).postRequest(URL,jsonObject.toString());
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().
                getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notif_id);

    }
}
