package com.healthapp.drgsgupta.service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.healthapp.drgsgupta.FirebaseConstants;
import com.navia.naviahelp.MyPrefs;

/**
 * Created by amitgupta on 11/14/17.
 */

public class FirebaseInstanceIdService extends com.google.firebase.iid.FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String token = FirebaseInstanceId.getInstance().getToken();
//        Log.d("firebase", token);
        MyPrefs.putString(this, FirebaseConstants.PREF_FIREBASE_TOKEN, token);
    }
}
