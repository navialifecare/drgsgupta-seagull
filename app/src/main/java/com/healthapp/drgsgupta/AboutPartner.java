package com.healthapp.drgsgupta;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


import java.util.List;
import java.util.Locale;

/**
 * Created by JaiGuruji on 20/03/17.
 */

public class AboutPartner extends AppCompatActivity {

    TextView tv_map;
    Double mLatitude,mLongitude;
    Activity mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_about_partner);

        mContext = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.app_bar);
        toolbar.setTitle("About Dr G.S. Gupta");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        tv_map=(TextView)findViewById(R.id.find_on_map);

        tv_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getAddressLocationForMap();

                Bundle bundle = new Bundle();
                bundle.putDouble("latitude",mLatitude);
                bundle.putDouble("longitude",mLongitude);
                Fragment fragment = new ClinicMapFragment();
                fragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.placeholder, fragment, "MAP").addToBackStack(null).commit();

            }
        });

    }

    private void getAddressLocationForMap(){
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        String mAddress_string = getString(R.string.organization_address);

        try {
            List<Address> addressList = geocoder.getFromLocationName(
                    mAddress_string
                    , 1);
            Address location = addressList.get(0);
            mLatitude =location.getLatitude();
            mLongitude = location.getLongitude();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        ClinicMapFragment fragment = (ClinicMapFragment)getSupportFragmentManager().findFragmentByTag("MAP");
        if (fragment != null && fragment.isVisible()) {
            Intent intent=new Intent(this,AboutPartner.class);
            startActivity(intent);
        }
        finish();
    }
}
