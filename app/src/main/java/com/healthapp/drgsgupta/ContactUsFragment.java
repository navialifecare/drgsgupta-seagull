package com.healthapp.drgsgupta;

import android.app.Activity;
import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ContactUsFragment extends AppCompatActivity {

    public EditText EmailBody;
    Spinner SubjectSpinner;
    private String Subject_line = "General";
    Activity mActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_contact_us);

        EmailBody = (EditText) findViewById(R.id.Feedback_message);
        SubjectSpinner = (Spinner) findViewById(R.id.subject_line);
        final Button sendMail = (Button) findViewById(R.id.send_button);

        List<String> Subject = new ArrayList<String>();
        Subject.add("Feedback");
        Subject.add("Issue Faced");
        Subject.add("General");

        mActivity = this;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, Subject);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SubjectSpinner.setAdapter(dataAdapter);

        SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Subject_line = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });


        sendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail(EmailBody.getText().toString());
            }
        });
    }

    public void sendEmail(String emailContent) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@navialifecare.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, Subject_line);
        i.putExtra(Intent.EXTRA_TEXT, emailContent);
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(mActivity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
