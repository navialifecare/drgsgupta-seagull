package com.healthapp.drgsgupta;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.healthapp.drgsgupta.student.StudentHomeActivity;
import com.healthapp.appointment_module.AppointmentDelegate;
import com.healthapp.appointment_module.AppointmentsListCallback;
import com.healthapp.broadcast_module.BroadcastDelegate;
import com.healthapp.chat_module.XmppChatDelegate;
import com.healthapp.doctor_departments_module.DoctorsDepartmentsCallback;
import com.healthapp.doctor_departments_module.DoctorsDepartmentsDelegate;
import com.healthapp.usermodule.UserDelegate;
import com.healthapp.usermodule.UserModuleCallback;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.HelperFunctions;
import com.navia.naviahelp.MyPrefs;
import com.squareup.picasso.Picasso;

import es.dmoral.toasty.Toasty;

/**
 * Created by piyushjain on 26/06/15.
 */
public class SplashActivity extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1500;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    String partner_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Uri uri = getIntent().getData();
        setContentView(R.layout.splash_layout);
        try {
            ImageView img = (ImageView) findViewById(R.id.imageView4);
            Picasso.get().load(R.drawable.app_icon).error(R.drawable.app_icon).into(img);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Bundle pushMessageBundle;
        String _for = null;

        if (getIntent().getExtras() != null) {
            pushMessageBundle = getIntent().getExtras();
            _for = pushMessageBundle.getString("for");
        }

        setXmppIPAddress();

        Typeface typeface = FontCache.getTypeFace(FontNamesClass.fontBold, this);
        TextView powered_by = (TextView) findViewById(R.id.tv_powered);
        powered_by.setTypeface(typeface);

        //google analytics
        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1);
        tracker = analytics.newTracker("UA-67527848-2"); // Replace with actual tracker/property Id UA-62416888-3
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);


        if (uri != null) {
            if (uri.getEncodedPath() != null) {
                if (uri.getEncodedPath().split("/")[3].equalsIgnoreCase(AppConstants.APP_NAME)) {
                    partner_name = uri.getEncodedPath().split("/")[3];
                    _for = "appointment_booking";
                } else {
                    setToasty(16);
                    Toasty.error(getApplicationContext(), "Please use " + uri.getEncodedPath().split("/")[3] + " app to generate Queue no.", Toast.LENGTH_LONG, true).show();
                }
            }
        }

        final String final_for = _for;
        if (final_for != null)
            Log.d("helloFriends", final_for);


        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with headingheader_box timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

                if (MyPrefs.getBoolean(SplashActivity.this, Constants.PREF_REGISTERED, false)) {
                    if (final_for != null) {
                        Intent i;
                        switch (final_for) {
                            case "broadcast":
                                BroadcastDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).startBroadcastModule(SplashActivity.this);
                                break;

                            case "appointment_booking":
                                if(partner_name!=null){
                                    launchAppointmentsThroughQR();
                                }
                                else {
                                    launchAppointmentsAndVisits();
                                }
                                break;

                            case "appointment_cancel":
                                launchAppointmentsAndVisits();
                                break;
                            default:
                                i = new Intent(SplashActivity.this, HomeActivity.class);
                                startActivity(i);
                                break;
                        }
                    }
                    else if (MyPrefs.getBoolean(SplashActivity.this, Constants.PREF_IS_STUDENT, false)) {
                        Intent intent = new Intent(SplashActivity.this, StudentHomeActivity.class);
                        startActivity(intent);
                    }
                    else {
                        Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(i);
                    }
                } else {
                    if (uri != null) {
                        if (uri.getEncodedPath().split("/")[3].equalsIgnoreCase(AppConstants.APP_NAME)) {
                            Toasty.error(getApplicationContext(), "Please login first to generate queue number for your appointment", Toast.LENGTH_LONG, true).show();
                        }
                    }

                    UserDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE,AppConstants.HOSPITAL_NAME).gotoUserSelectActivityThroughQR(SplashActivity.this, new UserModuleCallback() {
                        @Override
                        public void startHomeActivity() {
                            Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(i);
                        }

                        @Override
                        public void registerUserForXmpp() {
                            XmppChatDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).registerUser(SplashActivity.this);
                        }

                        @Override
                        public void enqueueXmppService() {
                            XmppChatDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).startXmppChat(SplashActivity.this);

                        }
                    },partner_name);
                }

                finish();
            }
        }, SPLASH_TIME_OUT);

    }

    private void setXmppIPAddress() {
        if (HelperFunctions.isInternetAvailable(getApplicationContext())) {
            XmppChatDelegate.getInstance(AppConstants.APP_NAME, AppConstants.VERSION_CODE)
                    .retrieveDnsIpAddress();
        }
    }

    private void setToasty(int size) {
        Toasty.Config.getInstance()
                .setErrorColor(getResources().getColor(R.color.red))
                .setTextColor(getResources().getColor(R.color.white))
                .tintIcon(true)
                .setTextSize(size)
                .apply();
    }


    private void launchAppointmentsAndVisits() {
        AppointmentDelegate.getInstance(AppConstants.APP_NAME, AppConstants.VERSION_CODE)
                .showUserAppointmentsList(SplashActivity.this, new AppointmentsListCallback() {
                    @Override
                    public void showDepartmentListing() {
                        /**
                         * The app may have departments, or just doctors, handle that in
                         * Doctor Department module
                         * If app belongs to only one doctor only, show slots directly
                         **/

                        AppointmentDelegate.getInstance(AppConstants.APP_NAME,
                                AppConstants.VERSION_CODE)
                                .initiateBookingForDoctor(SplashActivity.this,
                                        AppConstants.MEDICAL_DIRECTOR_NAME, AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER, "", "","");
                    }

                    @Override
                    public void startHomeActivity(Context context) {
                        Intent  intent=new Intent(context, HomeActivity.class);
                        context.startActivity(intent);
                    }
                });
    }

    private void launchAppointmentsThroughQR() {
        AppointmentDelegate.getInstance(AppConstants.APP_NAME, AppConstants.VERSION_CODE)
                .showUserAppointmentsListWithQR(SplashActivity.this, new AppointmentsListCallback() {
                    @Override
                    public void showDepartmentListing() {
                        /**
                         * The app may have departments, or just doctors, handle that in
                         * Doctor Department module
                         * If app belongs to only one doctor only, show slots directly
                         **/

                        AppointmentDelegate.getInstance(AppConstants.APP_NAME,
                                AppConstants.VERSION_CODE)
                                .initiateBookingForDoctor(SplashActivity.this,
                                        AppConstants.MEDICAL_DIRECTOR_NAME, AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER, "", "","");
                    }

                    @Override
                    public void startHomeActivity(Context context) {
                        Intent  intent=new Intent(context, HomeActivity.class);
                        context.startActivity(intent);
                    }
                });
    }
}