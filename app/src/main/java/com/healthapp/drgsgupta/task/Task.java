package com.healthapp.drgsgupta.task;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Defines a Task object (name, completion, priority, date created, due date, notes), 
 * provides multiple constructors as well as mutators, it has its own defined toString() and equals()
 * @ Paradeep Bishnoi
 */


public class Task {

	/**************************************************************************
	 * Static fields and methods                                              *
	 **************************************************************************/

	// Extra intent flags
	public static final String EXTRA_TASK_TYPE = "com.healthapp.navia.TASK";
	public static final String EXTRA_TASK_ID = "com.healthapp.navia.TASK_ID";
	public static final String EXTRA_TASK_IDS = "com.healthapp.navia.TASK_IDS";
	public static final String EXTRA_TASK_SEND_TO_HISTORY = "com.healthapp.navia.TASK_SEND_TO_HISTORY";
	public static final String EXTRA_TASK_NOTIFICATION_ID = "com.healthapp.navia.TASK_NOTIFICATION_ID";


	// Priority constants
	public static final String[] PRIORITY_LABELS = {"Trivial", "Normal", "Urgent"};
	public static final String[] REPEAT_LABELS = {"minutes", "hours", "days", "weeks", "months", "years"};
	public static final int TRIVIAL = 0;
	public static final int NORMAL = 1;
	public static final int URGENT = 2;

	// Repeat constants
	public static final int MINUTES = 0;
	public static final int HOURS = 1;
	public static final int DAYS = 2;
	public static final int WEEKS = 3;
	public static final int MONTHS = 4;
	public static final int YEARS = 5;

	/**************************************************************************
	 * Private fields                                                         *
	 **************************************************************************/

	private int id;
	private String name;
	private boolean isCompleted;
	private int priority;
	private int category;
	private boolean hasDateDue;
	private boolean hasFinalDateDue;

	private boolean isRepeating;
	private int repeatType;
	private int repeatInterval;
	private long dateCreated;
	private long dateModified;
	private long dateDue;
	private String gID;
	private String notes;
	private Calendar dateCreatedCal;
	private Calendar dateModifiedCal;
	private Calendar dateDueCal;
	private boolean isOngoing;
	private String remarks;
	private String dueDateString;

	/**************************************************************************
	 * Constructors                                                           *
	 **************************************************************************/

	/**
	 * Default constructor. Creates an empty task.
	 */
	public Task() {}

	/**
	 * Constructor, all fields
	 * @param id
	 * @param name
	 * @param isCompleted
	 * @param priority
	 * @param category
	 * @param hasDateDue
	 * @param hasFinalDateDue
	 * @param isRepeating
	 * @param repeatType
	 * @param repeatInterval
	 * @param dateCreated
	 * @param dateModified
	 * @param dateDue
	 * @param gID
	 * @param medicineName
	 * @param isOngoing
	 * @param DueDateString
	 */
	public Task(int id,
				String name,
				boolean isCompleted,
				int priority,
				int category,
				boolean hasDateDue,
				boolean hasFinalDateDue,
				boolean isRepeating,
				int repeatType,
				int repeatInterval,
				long dateCreated,
				long dateModified,
				long dateDue,
				String gID,
				String medicineName, boolean isOngoing, String remarks, String DueDateString) {
		this.id = id;
		this.name = name;
		this.isCompleted = isCompleted;
		this.priority = priority;
		this.category = category;
		this.hasDateDue = hasDateDue;
		this.hasFinalDateDue = hasFinalDateDue;
		this.isRepeating = isRepeating;
		this.repeatType = repeatType;
		this.repeatInterval = repeatInterval;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.dateDue = dateDue;
		this.gID = gID;
		this.notes = medicineName;
		this.isOngoing=isOngoing;
		this.remarks=remarks;
		this.dueDateString = DueDateString;

		updateDateCreatedCal();
		updateDateModifiedCal();
		updateDateDueCal();
	}

	/**************************************************************************
	 * Class methods                                                          *
	 **************************************************************************/

	private void updateDateCreatedCal() {
		if (dateCreatedCal == null)
			dateCreatedCal = new GregorianCalendar();

		dateCreatedCal.setTimeInMillis(dateCreated);
	}

	private void updateDateModifiedCal() {
		if (dateModifiedCal == null)
			dateModifiedCal = new GregorianCalendar();

		dateModifiedCal.setTimeInMillis(dateModified);
	}

	private void updateDateDueCal() {
		if (!hasDateDue)
		{
			dateDueCal = null;
			return;
		}

		if (dateDueCal == null)
			dateDueCal = new GregorianCalendar();

		dateDueCal.setTimeInMillis(dateDue);
	}

	public boolean isPastDue() {
		if (!hasDateDue || isCompleted)
			return false;

		return dateDue - System.currentTimeMillis() < 0;
	}

	public String getRemarks() {
		return remarks;
	}

	public String getDueDateString(){ return dueDateString;}
	public void setDueDateString(String dueDateString){ this.dueDateString = dueDateString;}

	/**************************************************************************
	 * Overridden parent methods                                              *
	 **************************************************************************/
	/**
	 * Compares this object to another. To return true, the compared object must
	 * have the same class and identical IDs.
	 * @param o the object to be compared with
	 * @return true if the objects are equal, false otherwise
	 */
	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (o == null)
			return false;
		if (o.getClass() != this.getClass())
			return false;

		return ((Task) o).id == this.id;
	}

	/**
	 * Returns a string representation of the class.
	 * @return a string representation of the class
	 */
	@Override
	public String toString() {
		return name;
	}

	/**************************************************************************
	 * Getters and setters                                                    *
	 **************************************************************************/

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(boolean is_completed) {
		this.isCompleted = is_completed;
	}

	public void toggleIsCompleted() {
		isCompleted = isCompleted ? false : true;
	}

	public int getPriority() {
		return priority;
	}

	public void setIsOngoing(boolean isOngoing) {
		this.isOngoing = isOngoing;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

//	//public void setIsOngoing(boolean isOngoing) {
//		this.isOngoing = isOngoing;
//	}



	public int getCategory() {
		return category;
	}

	public boolean isOngoing() {
		return isOngoing;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public boolean hasDateDue() {
		return hasDateDue;
	}

	public void setHasDateDue(boolean hasDateDue) {
		this.hasDateDue = hasDateDue;
	}

	public boolean hasFinalDateDue() {
		return hasFinalDateDue;
	}

	public void setHasFinalDateDue(boolean hasFinalDateDue) {
		this.hasFinalDateDue = hasFinalDateDue;
	}

	public boolean isRepeating() {
		return isRepeating;
	}

	public void setIsRepeating(boolean isRepeating) {
		this.isRepeating = isRepeating;
	}

	public int getRepeatType() {
		return repeatType;
	}

	public void setRepeatType(int repeatType) {
		if (repeatType >= 0 && repeatType <= 5) {
			this.isRepeating = true;
			this.repeatType = repeatType;
		}
	}
	//   public boolean isOngoing(){
//		 return isOngoing;
//	}
	public int getRepeatInterval() {
		return repeatInterval;
	}

	public void setRepeatInterval(int repeatInterval) {
		this.repeatInterval = repeatInterval;
	}

	public long getDateCreated() {
		return dateCreated;
	}

	public Calendar getDateCreatedCal() {
		return dateCreatedCal;
	}

	public void setDateCreated(long date_created) {
		this.dateCreated = date_created;
		updateDateCreatedCal();
	}

	public long getDateModified() {
		return dateModified;
	}

	public Calendar getDateModifiedCal() {
		return dateModifiedCal;
	}

	public void setDateModified(long date_modified) {
		this.dateModified = date_modified;
		updateDateModifiedCal();
	}

	public long getDateDue() {
		return dateDue;
	}

	public Calendar getDateDueCal() {
		return dateDueCal;
	}

	public void setDateDue(long date_due) {
		this.hasDateDue = true;
		this.dateDue = date_due;
		updateDateDueCal();
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getgID() {
		return gID;
	}

	public void setgID(String gID) {
		this.gID = gID;
	}



}
