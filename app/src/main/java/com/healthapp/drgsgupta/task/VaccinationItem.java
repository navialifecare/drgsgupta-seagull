package com.healthapp.drgsgupta.task;

import org.json.JSONArray;

/**
 * Created by JaiGuruji on 22/02/17.
 */

public class VaccinationItem  {

    String mStartDate;
    String mBabyName;
    String mDoctorName;
    JSONArray jsonArray;

    public VaccinationItem() {
    }


    public String getmBabyName() {
        return mBabyName;
    }

    public void setmBabyName(String mBabyName) {
        this.mBabyName = mBabyName;
    }

    public String getmDoctorName() {
        return mDoctorName;
    }

    public void setmDoctorName(String mDoctorName) {
        this.mDoctorName = mDoctorName;
    }

    public String getmStartDate() {
        return mStartDate;
    }

    public void setmStartDate(String mStartDate) {
        this.mStartDate = mStartDate;
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public void setJsonArray(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }
}
