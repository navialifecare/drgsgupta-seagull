package com.healthapp.drgsgupta.student;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class StudentContentAdapter extends RecyclerView.Adapter<StudentContentAdapter.StudentContentViewHolder> {

    private List<Response.Content> content;
    private Context context;

    public StudentContentAdapter(List<Response.Content> content, Context context) {
        this.content = content;
        this.context = context;
    }

    @Override
    public StudentContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.feed_item, parent, false);
        return new StudentContentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StudentContentViewHolder holder, final int position) {
        holder.title.setText(content.get(position).getTitle());
        try {
            Picasso.get().load(content.get(position).getImageLink()).error(R.drawable.bg_img_placeholder).into(holder.image);
        } catch (Exception e) {
            try {
                Picasso.get().load(content.get(position).getImageLink()).error(R.drawable.bg_img_placeholder).into(holder.image);
            } catch (Exception exp){

            }
        }

        holder.contentItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNewActivityWithFullContent(content.get(position).getTitle(),
                        content.get(position).getImageLink(),content.get(position).getCreationDate(),
                        content.get(position).getContentType(), content.get(position).getContent(), content.get(position).getFileLink());
            }
        });
    }

    private void openNewActivityWithFullContent(String title, String image, String time, String content_type, String content_text, String fileLink) {
        if(content_type.equals("Content")) {
            Intent intent = new Intent(context, StudentContentActivity.class);
            intent.putExtra("content_title", title);
            intent.putExtra("content_image", image);
            intent.putExtra("content_time", time);
            intent.putExtra("content", content_text);
            context.startActivity(intent);
        } else if (content_type.equals("Video")){
            //Show video
            String id = fileLink.split("v=")[1].substring(0,11);
            watchYoutubeVideo(context, id);
        }
    }

    public void watchYoutubeVideo(Context context, String id){
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + id));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }

    @Override
    public int getItemCount() {
        return content.size();
    }

    public class StudentContentViewHolder extends RecyclerView.ViewHolder{

        LinearLayout contentItem;
        ImageView image;
        TextView title;

        public StudentContentViewHolder(View itemView) {
            super(itemView);
            contentItem = (LinearLayout) itemView.findViewById(R.id.item_content);
            image = (ImageView) itemView.findViewById(R.id.content_img);
            title = (TextView) itemView.findViewById(R.id.content_title);
        }
    }
}
