package com.healthapp.drgsgupta.student.mvp;

public interface StudentHomePresenterContract {
    void registerForFcm();
    void loadStudentContent();
    void createStudentTag();
}
