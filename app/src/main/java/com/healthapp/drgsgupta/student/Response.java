package com.healthapp.drgsgupta.student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Shikhar on 3/22/2018.
 */

public class Response  {
    private String error_reason;
    private String result;

    public String getError_reason() {
        return error_reason;
    }

    public void setError_reason(String error_reason) {
        this.error_reason = error_reason;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    private List<Content> Content;
    private List<Content> Tips;
    private List<Content> Video;

    public List<Response.Content> getContent() {
        return Content;
    }

    public void setContent(List<Response.Content> content) {
        Content = content;
    }

    public List<Response.Content> getTips() {
        return Tips;
    }

    public void setTips(List<Response.Content> tips) {
        Tips = tips;
    }

    public List<Response.Content> getVideos() {
        return Video;
    }

    public void setVideos(List<Response.Content> video) {
        Video = video;
    }

    public class Content{
        @SerializedName("content")
        @Expose
        private String content;
        @SerializedName("content_type")
        @Expose
        private String contentType;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("file_link")
        @Expose
        private String fileLink;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("image_link")
        @Expose
        private String imageLink;
        @SerializedName("alias_mapping_id")
        @Expose
        private String aliasMappingId;
        @SerializedName("file")
        @Expose
        private String file;
        @SerializedName("partner")
        @Expose
        private String partner;
        @SerializedName("partner_id")
        @Expose
        private String partnerId;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getContentType() {
            return contentType;
        }

        public void setContentType(String contentType) {
            this.contentType = contentType;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getFileLink() {
            return fileLink;
        }

        public void setFileLink(String fileLink) {
            this.fileLink = fileLink;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getImageLink() {
            return imageLink;
        }

        public void setImageLink(String imageLink) {
            this.imageLink = imageLink;
        }

        public String getAliasMappingId() {
            return aliasMappingId;
        }

        public void setAliasMappingId(String aliasMappingId) {
            this.aliasMappingId = aliasMappingId;
        }

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public String getPartner() {
            return partner;
        }

        public void setPartner(String partner) {
            this.partner = partner;
        }

        public String getPartnerId() {
            return partnerId;
        }

        public void setPartnerId(String partnerId) {
            this.partnerId = partnerId;
        }
    }
}
