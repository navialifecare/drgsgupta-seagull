package com.healthapp.drgsgupta.student.mvp;

import com.healthapp.drgsgupta.student.Response;

import java.util.List;

public interface StudentHomeViewContract {
    void inflateContent(List<Response.Content> content);

    void onMetaApiSuccess(boolean showDialog, String message);

    void expireSession();

    void showError(String error_reason);

    void inflateVideos(List<Response.Content> events);

    void inflateTips(List<Response.Content> tips);
}
