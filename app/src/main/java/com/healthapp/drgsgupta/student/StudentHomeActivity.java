package com.healthapp.drgsgupta.student;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;
import com.healthapp.drgsgupta.Utility;
import com.healthapp.drgsgupta.student.mvp.StudentHomePresenter;
import com.healthapp.drgsgupta.student.mvp.StudentHomeViewContract;
import com.navia.naviahelp.Constants;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class StudentHomeActivity extends Activity implements StudentHomeViewContract {


    StudentHomePresenter presenter;
    RecyclerView recyclerViewContent, recyclerViewVideos;
    TextView tv_no_content, tv_no_videos;
    ImageView logout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_home);

        //Initialise views
        recyclerViewContent = (RecyclerView) findViewById(R.id.rv_feed);
        recyclerViewVideos = (RecyclerView) findViewById(R.id.rv_videos);
        tv_no_content = (TextView) findViewById(R.id.tv_no_content);
        tv_no_videos = (TextView) findViewById(R.id.tv_no_videos);
        logout = (ImageView) findViewById(R.id.logout) ;

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.logoutTasks(StudentHomeActivity.this);
            }
        });

        presenter = new StudentHomePresenter(this, StudentHomeActivity.this);

        //Make presenter calls
        presenter.registerForFcm();
        presenter.createStudentTag();
        presenter.loadStudentContent();

    }

    @Override
    public void inflateContent(List<Response.Content> content) {
        if (content != null) {
            StudentContentAdapter studentContentAdapter = new StudentContentAdapter(content, this);
            recyclerViewContent.setAdapter(studentContentAdapter);
            recyclerViewContent.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        } else {
            tv_no_content.setVisibility(View.VISIBLE);
            recyclerViewContent.setVisibility(View.GONE);
        }
    }

    @Override
    public void inflateVideos(List<Response.Content> videos) {
        if (videos != null) {
            StudentContentAdapter studentContentAdapter = new StudentContentAdapter(videos, this);
            recyclerViewVideos.setAdapter(studentContentAdapter);
            recyclerViewVideos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        } else {
            tv_no_videos.setVisibility(View.VISIBLE);
            recyclerViewVideos.setVisibility(View.GONE);
        }
    }

    @Override
    public void inflateTips(List<Response.Content> tips) {

    }

    @Override
    public void onMetaApiSuccess(boolean showDialog, String message) {
        if (showDialog) {
            final AlertDialog alertDialog = new AlertDialog.Builder(StudentHomeActivity.this)
                    .setMessage(message)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            System.exit(0);
                        }
                    })
                    .create();

            //Override show listener to make dialog uncancelable
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String APP_NAME = Constants.PACKAGE_NAME;
                            String URL = "market://details?id=";
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL + APP_NAME));
                            startActivity(intent);
                        }
                    });
                }
            });

            alertDialog.setCancelable(false);

            alertDialog.show();
        }
    }

    @Override
    public void expireSession() {
        Toasty.error(this, "Session expired. Please login again.").show();
    }

    @Override
    public void showError(String error_reason) {
        new AlertDialog.Builder(this)
                .setMessage(error_reason)
                .setPositiveButton("OK", null)
                .create()
                .show();
    }

}
