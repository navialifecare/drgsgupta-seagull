package com.healthapp.drgsgupta.student;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;
import com.squareup.picasso.Picasso;

public class StudentContentActivity extends AppCompatActivity {

    ImageView imageView;
    TextView title, contentText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_content);

        String contentTitle = getIntent().getStringExtra("content_title");
        String contentImage = getIntent().getStringExtra("content_image");
        String content = getIntent().getStringExtra("content");
        String contentTime = getIntent().getStringExtra("content_time");

        imageView = (ImageView) findViewById(R.id.image);
        title = (TextView) findViewById(R.id.title);
        contentText = (TextView) findViewById(R.id.text);

        Picasso.get().load(contentImage).error(R.drawable.bg_img_placeholder).into(imageView);
        title.setText(contentTitle);
        contentText.setText(content);

    }
}
