package com.healthapp.drgsgupta.student.mvp;


import android.app.Activity;
import android.content.Intent;

import com.healthapp.drgsgupta.SplashActivity;
import com.healthapp.drgsgupta.Utility;
import com.healthapp.drgsgupta.new_apis.meta.AppForceUpdateResponse;
import com.healthapp.drgsgupta.student.Response;
import com.navia.naviahelp.Constants;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class StudentHomePresenter implements StudentHomePresenterContract{

    private StudentHomeViewContract mView;
    private Activity activity;
    private StudentHomeModel model;

    public StudentHomePresenter(StudentHomeViewContract mView,Activity activity) {
        this.mView = mView;
        this.activity = activity;
        this.model = new StudentHomeModel();
    }

    @Override
    public void registerForFcm() { }

    @Override
    public void loadStudentContent() {
        Observable.defer(new Callable<ObservableSource<Response>>() {
            @Override
            public ObservableSource<Response> call() throws Exception {
                return Observable.just(model.fetchStudentContent());
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableObserver<Response>() {
                    @Override
                    public void onNext(Response contentResponse) {
                        switch (contentResponse.getResult()) {
                            case "SUCCESS":
                                mView.inflateContent(contentResponse.getContent());
                                mView.inflateVideos(contentResponse.getVideos());
                                mView.inflateTips(contentResponse.getTips());
                                break;
                            case "FAILURE":
                                mView.showError(contentResponse.getError_reason());
                                break;
                            default:
                                mView.showError("Something went wrong");
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError(e.getLocalizedMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void createStudentTag() {
        Observable.defer(new Callable<ObservableSource<String>>() {
            @Override
            public ObservableSource<String> call() throws Exception {
                return Observable.just(model.createStudentTag());
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<String>() {
                    @Override
                    public void onNext(String s) {
                        if(s.equals(Constants.AUTH_TOKEN_REFRESH_REQUIRED)){
                            if(activity!=null)
                            Utility.logoutTasks(activity);
                            mView.expireSession();
                        } else {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
