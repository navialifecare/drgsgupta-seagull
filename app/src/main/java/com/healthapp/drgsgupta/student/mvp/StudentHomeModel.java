package com.healthapp.drgsgupta.student.mvp;

import com.google.gson.Gson;

import com.healthapp.drgsgupta.AppConstants;
import com.healthapp.drgsgupta.MyApplication;
import com.healthapp.drgsgupta.student.Response;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.MyPrefs;
import com.navia.naviahelp.OkhttpClientManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StudentHomeModel {
    public Response fetchStudentContent() throws IOException {

        //TODO: Tag ki key pata karo bro!
        String url = Constants.NEW_TEST_URL + "content/?partner=" + AppConstants.APP_NAME + "&tag=Student&filterby=user";
        String content = OkhttpClientManager.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).getRequest(url);
        Response contentResponse = null;

        if (content.equals(Constants.AUTH_TOKEN_REFRESH_REQUIRED)) {
            contentResponse = new Response();
            contentResponse.setResult(Constants.AUTH_TOKEN_REFRESH_REQUIRED);
        } else {
            contentResponse = new Gson().fromJson(content,Response.class);
        }

        return contentResponse;
    }

    public String createStudentTag() throws IOException, JSONException {
        String url = Constants.NEW_TEST_URL + "navia/tags";
        List<String> tags = new ArrayList<>();
        tags.add("Student");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("for_number", MyPrefs.getString(MyApplication.getContext(), Constants.PHONE_ID, ""));
        jsonObject.put("doctor_number", AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER);
        jsonObject.put("tags", new JSONArray(tags));

        return OkhttpClientManager.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).postRequest(url, jsonObject.toString());
    }
}
