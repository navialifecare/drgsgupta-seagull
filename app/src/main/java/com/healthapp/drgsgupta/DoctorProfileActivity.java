package com.healthapp.drgsgupta;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.healthapp.appointment_module.AppointmentDelegate;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.HelperFunctions;
import com.navia.naviahelp.OkhttpClientManager;
import com.squareup.picasso.Picasso;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;

/**
 * Created by amitgupta on 9/25/17.
 */

public class DoctorProfileActivity extends AppCompatActivity {
    Activity mActivity;
    TextView tv_label_experience, tv_experience, tv_label_education, tv_education, tv_label_clinics;
    TextView tv_address, tv_label_contact, tv_phone, tv_email, tv_name, tv_specialization, tv_find_on_map;
    TextView tv_label_days, tv_label_timings, tv_working_days, tv_timings, tv_website;
    RelativeLayout rl_doctor_profile, rl_progress;
    Disposable disposable;
    DoctorProfileResponse.DoctorProfileData doctorProfileData;
    ImageView img_doctor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        toolbar.setTitle("Doctor Profile");
        setSupportActionBar(toolbar);
        HelperFunctions.changeToolbarTitle(toolbar, FontNamesClass.fontBold);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mActivity = this;

        Typeface bold = FontCache.getTypeFace(FontNamesClass.fontBold, mActivity);
        Typeface regular = FontCache.getTypeFace(FontNamesClass.fontRegular, mActivity);

        tv_label_experience = (TextView) findViewById(R.id.tv_label_experience);
        tv_experience = (TextView) findViewById(R.id.tv_experience);
        tv_label_education = (TextView) findViewById(R.id.tv_label_education);
        tv_education = (TextView) findViewById(R.id.tv_education);
        tv_label_clinics = (TextView) findViewById(R.id.tv_label_clinics);
        tv_address = (TextView) findViewById(R.id.tv_clinics);
        tv_label_contact = (TextView) findViewById(R.id.tv_contact);
        tv_phone = (TextView) findViewById(R.id.tv_phone);
        tv_email = (TextView) findViewById(R.id.tv_email);
        tv_name = (TextView) findViewById(R.id.tv_doctor_name);
        tv_specialization = (TextView) findViewById(R.id.tv_specialisation);
        tv_find_on_map = (TextView) findViewById(R.id.tv_find_on_map);
        tv_label_days = (TextView) findViewById(R.id.tv_label_days);
        tv_label_timings = (TextView) findViewById(R.id.tv_label_timings);
        tv_working_days = (TextView) findViewById(R.id.tv_working_days);
        tv_timings = (TextView) findViewById(R.id.tv_timings);
        rl_doctor_profile = (RelativeLayout) findViewById(R.id.rl_doctor_profile);
        tv_website = (TextView) findViewById(R.id.tv_website);

        rl_progress = (RelativeLayout) findViewById(R.id.rl_progress);


        img_doctor = (ImageView) findViewById(R.id.img_doctor);
        try {
            Picasso.get().load(R.drawable.image_home1).into(img_doctor);
        }catch (Exception e){
            e.printStackTrace();
            img_doctor.setImageResource(R.drawable.image_home1);
        }

        tv_website.setText(Html.fromHtml("<a href=\"www.sarwalheartsurgery.com\">www.sarwalheartsurgery.com</a>"));
        tv_website.setMovementMethod(LinkMovementMethod.getInstance());

        tv_label_experience.setTypeface(bold);
        tv_label_experience.setTypeface(bold);
        tv_label_education.setTypeface(bold);
        tv_education.setTypeface(regular);
        tv_label_clinics.setTypeface(bold);
        tv_address.setTypeface(regular);
        tv_label_contact.setTypeface(bold);
        tv_phone.setTypeface(regular);
        tv_email.setTypeface(regular);
        tv_experience.setTypeface(regular);
        tv_name.setTypeface(bold);
        tv_specialization.setTypeface(regular);
        tv_find_on_map.setTypeface(bold);
        tv_timings.setTypeface(regular);
        tv_label_timings.setTypeface(bold);
        tv_timings.setTypeface(regular);
        tv_label_timings.setTypeface(bold);
        tv_website.setTypeface(regular);

        getDoctorProfile1();

    }




    public void getDoctorProfile1(){
       rl_progress.setVisibility(View.VISIBLE);



        final String URL =  Constants.URL+"doctor/";
        final HttpUrl httpUrl = HttpUrl.parse(URL).
                newBuilder()
                .addQueryParameter("doctor_number",AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER)
                .build();



        disposable= Observable.defer(new Callable<ObservableSource<String>>() {
            @Override
            public ObservableSource<String> call() throws Exception {
                return Observable.just(OkhttpClientManager.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).getRequest(httpUrl.toString()));
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<String>(){
                    @Override
                    public void onNext(@NonNull String response) {
                        if(response.equals(Constants.AUTH_TOKEN_REFRESH_REQUIRED)){
                            return;
                        }

                        rl_doctor_profile.setVisibility(View.VISIBLE);

                        rl_progress.setVisibility(View.GONE);
                        DoctorProfileResponse doctorProfileResponse = new Gson().fromJson(response, DoctorProfileResponse.class);

                        if(doctorProfileResponse.getResult().equals("SUCCESS")) {
                            setDataToFields(doctorProfileResponse);
                            doctorProfileData = doctorProfileResponse.getDoctorProfileData();
                        }
                        else{
                            AlertDialog alertDialog = new AlertDialog.Builder(mActivity)
                                    .setMessage(getString(R.string.error_refresh_no_internet))
                                    .create();
                            alertDialog.show();

                        }

                    }


                    @Override
                    public void onError(@NonNull Throwable e) {
                        HelperFunctions.displayDialog(mActivity,getString(R.string.SomethingWentWrong));
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }



    void setDataToFields(DoctorProfileResponse doctorProfileResponse) {
        DoctorProfileResponse.DoctorProfileData doctorProfileData = doctorProfileResponse.getDoctorProfileData();

        String address = doctorProfileData.getAddress();
        String specialisation = doctorProfileData.getSpecialization();
        String name = doctorProfileData.getDoctorName();
        String phone = AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER;
        String email = doctorProfileData.getEmail();
        String days = doctorProfileData.getWorkingDays();
        String timings_from = doctorProfileData.getTimeFrom();
        String timings_to = doctorProfileData.getTimeTo();
        String doctor_brief = doctorProfileData.getBiography();

        tv_address.setText(address);
        tv_specialization.setText(specialisation);
        tv_name.setText(name);
        tv_timings.setText(timings_from + "to" + timings_to);
        tv_phone.setText(phone);
        tv_email.setText(email);
        tv_working_days.setText(days);
        tv_experience.setText(doctor_brief);

        try {
            Picasso.get().load(doctorProfileData.getImage()).error(R.drawable.image_home1).into(img_doctor);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}
