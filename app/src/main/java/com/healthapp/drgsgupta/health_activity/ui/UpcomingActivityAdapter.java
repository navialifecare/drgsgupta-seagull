package com.healthapp.drgsgupta.health_activity.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;
import com.healthapp.drgsgupta.health_activity.data.ActivityType;
import com.healthapp.drgsgupta.health_activity.data.HealthActivity;

import java.util.Collections;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Created by Shikhar on 3/8/2018.
 */

public class UpcomingActivityAdapter extends RecyclerView.Adapter<UpcomingActivityAdapter.UpcomingViewHolder> {

    private List<HealthActivity> healthActivityList;
    private Context context;

    public UpcomingActivityAdapter(Context context, List<HealthActivity> healthActivityList) {
        this.context = context;
        this.healthActivityList = healthActivityList;
        Collections.sort(healthActivityList);
        Log.d("test", healthActivityList.toString());

    }

    @Override
    public UpcomingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.upcoming_activity_item, parent, false);
        return new UpcomingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UpcomingViewHolder holder, int position) {

        if(healthActivityList.size()>0) {

            holder.activityName.setText(healthActivityList.get(position).getName());
            holder.activityTime.setText(healthActivityList.get(position).getTime());
            ActivityType type = healthActivityList.get(position).getType();
            if (type == ActivityType.TREATMENT) {
                holder.activityTypeIcon.setImageResource(R.drawable.ic_pills2);
                holder.activityTypeIcon.setBackgroundResource(R.drawable.bg_circle3);
            } else if (type == ActivityType.APPOINTMENT) {
                holder.activityTypeIcon.setImageResource(R.drawable.ic_calendar7);
                holder.activityTypeIcon.setBackgroundResource(R.drawable.bg_circle_orange);
            } else if (type == ActivityType.DIET) {
                holder.activityTypeIcon.setImageResource(R.drawable.diet1);
                holder.activityTypeIcon.setBackgroundResource(R.drawable.bg_circle1);
            } else if (type == ActivityType.VITAL_LOG) {
                holder.activityTypeIcon.setImageResource(R.drawable.bg_body_vital);
                holder.activityTypeIcon.setBackgroundResource(R.drawable.bg_circle2);

            }
        } else {
            Toasty.info(context, "No activities for this day").show();
        }
    }

    @Override
    public int getItemCount() {
        return healthActivityList.size();
    }

    class UpcomingViewHolder extends RecyclerView.ViewHolder {

        TextView activityName, activityTime;
        ImageView activityTypeIcon;

        public UpcomingViewHolder(View itemView) {
            super(itemView);
            activityName = (TextView) itemView.findViewById(R.id.activity_name);
            activityTime = (TextView) itemView.findViewById(R.id.activity_time);
            activityTypeIcon = (ImageView) itemView.findViewById(R.id.activity_icon);
        }
    }
}
