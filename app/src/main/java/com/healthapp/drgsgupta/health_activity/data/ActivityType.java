package com.healthapp.drgsgupta.health_activity.data;

/**
 * Created by Shikhar on 3/9/2018.
 */

public enum ActivityType {
    TREATMENT, VITAL_LOG, APPOINTMENT, DIET
}
