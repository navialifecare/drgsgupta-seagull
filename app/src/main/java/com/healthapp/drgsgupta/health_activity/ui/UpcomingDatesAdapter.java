package com.healthapp.drgsgupta.health_activity.ui;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Shikhar on 3/12/2018.
 */

public class UpcomingDatesAdapter extends RecyclerView.Adapter<UpcomingDatesAdapter.UpcomingDatesViewHolder> {

    private Activity activity;
    public int selectedDatePosition;
    private List<Long> dates;

    public UpcomingDatesAdapter(Activity activity, int selectedDatePosition) {
        this.activity = activity;

        //Since index can never be -1
        this.selectedDatePosition = -1;
        dates = new ArrayList<>();
        long now = System.currentTimeMillis();
        String startDate = new SimpleDateFormat("dd MM yyyy").format(new Date(now));
        long startMillis = 0;
        try {
            startMillis = new SimpleDateFormat("dd MM yyyy").parse(startDate).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dates.add(startMillis);
        for (int i = 0; i < 60; i++) {
            Long lastDate = dates.get(i);
            dates.add(lastDate + 24 * 60 * 60 * 1000);
        }
    }

    @Override
    public UpcomingDatesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.upcoming_date_layout, parent, false);
        return new UpcomingDatesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final UpcomingDatesViewHolder holder, final int position) {
        holder.date.setText(new SimpleDateFormat("dd").format(new Date(dates.get(position))));
        holder.monthAndYear.setText(new SimpleDateFormat("MMM, yyyy").format(new Date(dates.get(position))));

        holder.dateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedDatePosition = position;
                if (activity instanceof UpcomingActivity) {
                    ((UpcomingActivity) activity).currentDateMillis = getSelectedDate(position);
                    ((UpcomingActivity) activity).tomorrowDateMillis = ((UpcomingActivity) activity).currentDateMillis + 24 * 60 * 60 * 1000;
                    ((UpcomingActivity) activity).setHealthAdapterWithHealthActivities(false);
                    ((UpcomingActivity) activity).showAll.setVisibility(View.VISIBLE);
                }
                notifyDataSetChanged();
            }
        });

        if (selectedDatePosition == position) {
            holder.dateLayout.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.date.setTextColor(Color.parseColor("#2d354b"));
            holder.monthAndYear.setTextColor(Color.parseColor("#2d354b"));
        } else {
            holder.dateLayout.setBackgroundColor(Color.parseColor("#2d354b"));
            holder.date.setTextColor(Color.parseColor("#ffffff"));
            holder.monthAndYear.setTextColor(Color.parseColor("#ffffff"));
        }

    }

    private long getSelectedDate(int position) {
        return dates.get(position);
    }

    @Override
    public int getItemCount() {
        return dates.size();
    }

    class UpcomingDatesViewHolder extends RecyclerView.ViewHolder {

        TextView date;
        TextView monthAndYear;
        LinearLayout dateLayout;

        public UpcomingDatesViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.upcoming_date);
            monthAndYear = (TextView) itemView.findViewById(R.id.upcoming_month_and_year);
            dateLayout = (LinearLayout) itemView.findViewById(R.id.dates_layout);
        }
    }
}
