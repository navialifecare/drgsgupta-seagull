package com.healthapp.drgsgupta.health_activity.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.healthapp.appointment_module.AppointmentDelegate;
import com.healthapp.drgsgupta.AppConstants;
import com.healthapp.drgsgupta.MyApplication;
import com.healthapp.drgsgupta.R;
import com.healthapp.drgsgupta.SplashActivity;
import com.healthapp.drgsgupta.Utility;
import com.healthapp.drgsgupta.health_activity.data.ActivityType;
import com.healthapp.drgsgupta.health_activity.data.HealthActivity;
import com.healthapp.appointment_module.AppointmentFetchResponse;
import com.healthapp.appointment_module.AppointmentsListItem;
import com.navia.modules.treatmentmodule.data.database.TasksDataSource;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.HelperFunctions;
import com.navia.naviahelp.MyPrefs;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import es.dmoral.toasty.Toasty;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class UpcomingActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recyclerView;
    RecyclerView datesRecyclerView;
    LinearLayout emptyLayout;
    TextView showAll;
    private List<HealthActivity> healthActivities;
    long currentDateMillis;
    long tomorrowDateMillis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        toolbar.setTitle("Upcoming Activities");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.rv_upcoming);
        datesRecyclerView = (RecyclerView) findViewById(R.id.rv_dates);
        emptyLayout = (LinearLayout) findViewById(R.id.empty_layout);
        showAll = (TextView) findViewById(R.id.show_all);

        showAll.setOnClickListener(this);

        healthActivities = new ArrayList<>();

        long nowTime = System.currentTimeMillis();
        String todayDate = HelperFunctions.convertLongToDate(nowTime, 4);
        currentDateMillis = HelperFunctions.convertDateStringTolong(todayDate, 1);
        tomorrowDateMillis = currentDateMillis + 24 * 60 * 60 * 1000;

        setAdapterForDates();

        getAllTreatmentTasks();
        getAllAppointmentTasks();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.show_all){
            UpcomingDatesAdapter adapter = (UpcomingDatesAdapter) datesRecyclerView.getAdapter();

            showAll.setVisibility(View.INVISIBLE);
            //since index will never be -1
            adapter.selectedDatePosition = -1;
            adapter.notifyDataSetChanged();
            setHealthAdapterWithHealthActivities(true);
        }
    }

    private void setAdapterForDates() {
        UpcomingDatesAdapter adapter = new UpcomingDatesAdapter(this, 0);
        datesRecyclerView.setAdapter(adapter);
        datesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    void setHealthAdapterWithHealthActivities(boolean showAll) {
        if (healthActivities.size() > 0) {
            Log.d("test", healthActivities.toString());

            recyclerView.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);

            if(showAll){
                setAdapterWith(healthActivities);
            } else {

                List<HealthActivity> inflatableList = new ArrayList<>();
                for (HealthActivity healthActivity :
                        healthActivities) {
                    if (healthActivity.getTimeMillis() > currentDateMillis && healthActivity.getTimeMillis() < tomorrowDateMillis) {
                        healthActivity.setTime(HelperFunctions.convertLongToDate(healthActivity.getTimeMillis(), 2));
                        inflatableList.add(healthActivity);
                    }
                }
                if(inflatableList.size()>0) {
                    setAdapterWith(inflatableList);
                } else {
//                    Toasty.error(this, "No activities").show();
                    recyclerView.setVisibility(View.GONE);
                    emptyLayout.setVisibility(View.VISIBLE);
                }
            }
        } else {
//            Toasty.error(this, "No activities").show();
            recyclerView.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
        }
    }

    private void setAdapterWith(List<HealthActivity> inflatableList) {
        UpcomingActivityAdapter upcomingActivityAdapter = new UpcomingActivityAdapter(this, inflatableList);
        recyclerView.setAdapter(upcomingActivityAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void getAllAppointmentTasks() {
        JSONObject jsonObject = new JSONObject();
        String phoneNumber = MyPrefs.getString(this, Constants.PHONE_ID, "");
        try {
            jsonObject.put("patient_number", MyPrefs.getString(this, Constants.PHONE_ID, ""));
            jsonObject.put("appName", AppConstants.APP_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        ApiManager.getInstance().getMyAppointments(jsonObject, this);

        Observable.defer(new Callable<ObservableSource<AppointmentFetchResponse>>() {
            @Override
            public ObservableSource<AppointmentFetchResponse> call() throws Exception {
                return Observable.just(AppointmentDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).getAppointments( MyPrefs.getString(getApplicationContext(), Constants.PHONE_ID, "")));
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AppointmentFetchResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(AppointmentFetchResponse appointmentFetchResponse) {
                        if (appointmentFetchResponse.getResult().equals(Constants.AUTH_TOKEN_REFRESH_REQUIRED)){
                            Utility.logoutTasks(UpcomingActivity.this);
                            //Show application level toast!
                            Toasty.error(MyApplication.getContext(), "Session expired, please login again").show();
                            Intent intent = new Intent(UpcomingActivity.this, SplashActivity.class);
                            startActivity(intent);
                            finish();
                        } else if (appointmentFetchResponse.getResult().equals("SUCCESS")){
                            List<AppointmentFetchResponse.AppointmentDataItem> appointmentDataItems = appointmentFetchResponse.getData();
                            for (AppointmentFetchResponse.AppointmentDataItem appointmentDataItem : appointmentDataItems){
                                AppointmentsListItem appointmentsListItem = appointmentDataItem.getAppointmentsListItem();
                                HealthActivity healthActivity = new HealthActivity();
                                healthActivity.setName("Appointment with Dr. " + appointmentsListItem.getDoctorName());
                                healthActivity.setTime(appointmentsListItem.getBookingDate() + " " + appointmentsListItem.getBookingTime());
                                healthActivity.setTimeMillis(HelperFunctions.convertDateStringTolong(appointmentsListItem.getBookingDate() + " " + appointmentsListItem.getBookingTime(), 7));
                                healthActivity.setType(ActivityType.APPOINTMENT);
                                if (healthActivity.getTimeMillis() > System.currentTimeMillis() && appointmentsListItem.getDoctorName().toLowerCase().contains("gupta"))
                                    healthActivities.add(healthActivity);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        setHealthAdapterWithHealthActivities(true);
                    }

                    @Override
                    public void onComplete() {
                        setHealthAdapterWithHealthActivities(true);
                    }
                });

    }


    private void getAllTreatmentTasks() {
        ArrayList<com.navia.modules.treatmentmodule.data.Task> tasks = TasksDataSource.getInstance(MyApplication.context).getAllTasks();
        for (int i = 0; i < tasks.size(); i++) {
            HealthActivity healthActivity = new HealthActivity();
            healthActivity.setName(tasks.get(i).getName() + " (" + tasks.get(i).getNotes() + ")");
            healthActivity.setTimeMillis(tasks.get(i).getDateDue());
            healthActivity.setTime(HelperFunctions.convertLongToDate(healthActivity.getTimeMillis(), 2));
            healthActivity.setType(ActivityType.TREATMENT);
            if (healthActivity.getTimeMillis() > System.currentTimeMillis() )
                healthActivities.add(healthActivity);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return true;
    }

}
