package com.healthapp.drgsgupta.health_activity.data;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Comparator;

/**
 * Created by Shikhar on 3/8/2018.
 */

public class HealthActivity implements Comparable<HealthActivity> {

    private String name;
    private ActivityType type;
    private String time;
    private long timeMillis;
    private String date;

    public HealthActivity() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ActivityType getType() {
        return type;
    }

    public void setType(ActivityType type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public long getTimeMillis() {
        return timeMillis;
    }

    public void setTimeMillis(long timeMillis) {
        this.timeMillis = timeMillis;
    }

    @Override
    public String toString() {
        return "{time: " + time + "}";
    }

    @Override
    public int compareTo(@NonNull HealthActivity healthActivity) {
        return this.getTimeMillis() < healthActivity.getTimeMillis() ? -1 :
                this.getTimeMillis() > healthActivity.getTimeMillis() ? 1 : 0;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
