package com.healthapp.drgsgupta.new_apis.meta;


import com.healthapp.drgsgupta.new_apis.Response;

/**
 * Created by Shikhar on 4/6/2018.
 */

public class AppForceUpdateResponse extends Response {
    private Boolean forceUpdate;

    public Boolean getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(Boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }
}
