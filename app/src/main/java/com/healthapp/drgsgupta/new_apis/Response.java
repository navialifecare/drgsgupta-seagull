package com.healthapp.drgsgupta.new_apis;

/**
 * Created by Shikhar on 3/19/2018.
 */

public class Response {
    private String error_reason;
    private String result;

    public String getError_reason() {
        return error_reason;
    }

    public void setError_reason(String error_reason) {
        this.error_reason = error_reason;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
