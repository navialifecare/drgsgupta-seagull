package com.healthapp.drgsgupta.Utils;

import android.app.Application;
import android.content.Context;

import com.amplitude.api.Amplitude;

/**
 * Created by Shourjo on 1/19/16.
 */
public class Analytics {


    // Call getApplication to pass to parameter application
    public static void initAnalytics(Context context, Application application){
        Amplitude.getInstance().initialize(context,
                "23cde886e7f915d7b88c11597258f040").enableForegroundTracking((Application) application);
    }

}
