package com.healthapp.drgsgupta;

import android.content.Context;
import android.content.Intent;

import com.healthapp.chat_module.XmppChatDelegate;
import com.navia.modules.treatmentmodule.data.Task;
import com.navia.modules.treatmentmodule.data.database.TasksDataSource;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.FirebaseConstants;
import com.navia.naviahelp.MyApplication;
import com.navia.naviahelp.MyPrefs;

import java.util.ArrayList;

public class Utility {


    public static void logoutTasks(Context context) {

        String firebaseToken = MyPrefs.getString(com.navia.naviahelp.MyApplication.getContext(),
                com.navia.naviahelp.FirebaseConstants.PREF_FIREBASE_TOKEN, "");

        // Perform logout tasks
        MyPrefs.clear(com.navia.naviahelp.MyApplication.getContext());

        MyPrefs.putString(com.navia.naviahelp.MyApplication.getContext(), FirebaseConstants.PREF_FIREBASE_TOKEN, firebaseToken);
        MyPrefs.putBoolean(com.navia.naviahelp.MyApplication.getContext(), Constants.OPENED_ONCE, true);
        int currentVersionCode = 0;
        try {
            currentVersionCode = com.navia.naviahelp.MyApplication.getContext().getPackageManager().
                    getPackageInfo(com.navia.naviahelp.MyApplication.getContext().getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            // handle exception
            e.printStackTrace();
            return;
        }
        MyPrefs.putInt(com.navia.naviahelp.MyApplication.getContext(), Constants.VERSION_CODE_KEY, currentVersionCode);
        deleteTreatment();


        XmppChatDelegate.getInstance(AppConstants.APP_NAME, AppConstants.VERSION_CODE).stopXmppChat(context);

        Intent intent = new Intent(context, SplashActivity.class);
        context.startActivity(intent);
    }

    private static void deleteTreatment() {
        TasksDataSource db = TasksDataSource.getInstance(MyApplication.getContext()); //get access to the instance of TasksDataSource
        com.healthapp.body_vital_module.database.TasksDataSource db2=com.healthapp.body_vital_module.database.TasksDataSource.getInstance(com.healthapp.drgsgupta.MyApplication.context);
        ArrayList<Task> tasks = db.getAllTasks(); //Get a list of all the tasks there

        // Delete all treatments
        db.deleteAllTasks();

        //Delete all follow ups
        int deleted = db.deleteAllFolUpTasks();
        //Delete all side effects
        deleted = db.deleteAllSideEffects();
        deleted = db.deleteAllParameters();
        db2.deleteAllParameters();
    }

}
