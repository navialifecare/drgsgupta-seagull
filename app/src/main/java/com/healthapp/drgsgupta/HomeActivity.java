package com.healthapp.drgsgupta;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.codemybrainsout.ratingdialog.RatingDialog;
import com.github.javiersantos.appupdater.AppUpdater;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.healthapp.drgsgupta.NaviaContentActivity.NaviaContentActivity;
import com.healthapp.drgsgupta.Utils.Analytics;
import com.healthapp.drgsgupta.new_apis.meta.AppForceUpdateResponse;
import com.healthapp.chat_module.XmppChatDelegate;
import com.healthapp.promotion_module.PromotionCallback;
import com.healthapp.promotion_module.PromotionDelegate;
import com.healthapp.promotion_module.PromotionResponse;
import com.navia.modules.treatmentmodule.TreatmentModuleDelegate;
import com.navia.modules.treatmentmodule.data.database.TasksDataSource;
import com.navia.modules.treatmentmodule.data.network.UpdateHelper;
import com.navia.modules.treatmentmodule.data.service.OnDayReceiver;
import com.navia.modules.treatmentmodule.ui.TreatmentBuilder;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.HelperFunctions;
import com.navia.naviahelp.MyPrefs;
import com.navia.naviahelp.OkhttpClientManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import es.dmoral.toasty.Toasty;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;
    boolean doubleBackToExitPressedOnce = false;
    private DrawerLayout mDrawerLayout;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    Context contex = null;
    private PendingIntent pendingIntent;
    private DrawerLayout drawerlayout;
    private BroadcastReceiver mBroadcastreceiver;
    Fragment fragment;
    Disposable disposable;
    private boolean isNewVisit = false;
    BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contex = this;
        registerForFCM();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // window.setStatusBarColor(this.getResources().getColor(R.color.deep_blue));
        }

        Analytics.initAnalytics(contex, getApplication());


        // check if update exist then show user a dialog
        AppUpdater appUpdater = new AppUpdater(this).
                setContentOnUpdateAvailable("The new version of " + getString(R.string.app_name) +
                        " app is available .Please update the app to enjoy a better experience.").
                setButtonUpdate("Update Now")
                .showEvery(5);
        appUpdater.start();

        setContentView(R.layout.activity_home);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_view);
        switchToFragment(new HomeFragmentNew(), FragmentConstants.HOMEFRAGMENT);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        if (TreatmentBuilder.tpd != null) {
            TreatmentBuilder.tpd = null;
        }

        //register for Xmpp

        HelperFunctions.checkFirstRun(HomeActivity.this);


        mBroadcastreceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(GCMIntentService.REGISTRATION_SUCCESS)) {
                    String token = intent.getStringExtra("token");
                    MyPrefs.putString(HomeActivity.this, token, MyPrefs.getString(HomeActivity.this, Constants.PHONE_ID, ""));
                    //Toast.makeText(getApplicationContext(),"token" + token,Toast.LENGTH_LONG).show();
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("dev_id", MyPrefs.getString(HomeActivity.this, Constants.PHONE_ID, ""));
                        jsonObject.put("reg_id", token);
                        jsonObject.put("name", MyPrefs.getString(HomeActivity.this, Constants.PREF_FIRST_NAME, ""));
                        jsonObject.put("username", MyPrefs.getString(getApplicationContext(), Constants.PHONE_ID, ""));
                        jsonObject.put("device_id", MyPrefs.getString(getApplicationContext(), Constants.PREF_PIN, ""));

                    } catch (JSONException e) { }
                    String TempUrl = Constants.URL;
                    TempUrl = TempUrl.replace("navia/", "");
                    String URL = TempUrl + "messaging/gcm_register";
                    AsynctaskGCM asynctaskGCM = new AsynctaskGCM(HomeActivity.this, jsonObject.toString());
                    asynctaskGCM.execute(URL);


                    System.out.print(token);
                    Log.v("token", token);
                } else if (intent.getAction().equals(GCMIntentService.REGISTRATION_ERROR)) {
                    //Toast.makeText(getApplicationContext(),"GCM token Error",Toast.LENGTH_LONG).show();
                }
            }
        };

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultcode = apiAvailability.isGooglePlayServicesAvailable(getApplicationContext());
        if (ConnectionResult.SUCCESS != resultcode) {
            if (apiAvailability.isUserResolvableError(resultcode)) {
                Toast.makeText(getApplicationContext(), "Google Play Services is not installed", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Device doesn't support Google Play", Toast.LENGTH_LONG).show();
            }
        } else {
            Intent intent = new Intent(this, GCMIntentService.class);
            startService(intent);
        }

        // Retrieve a PendingIntent that will perform a broadcast
        Intent alarmIntent = new Intent(getApplicationContext(), OnDayReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, alarmIntent, 0);
        StartSchedule(getApplicationContext());
        XmppChatDelegate.getInstance(AppConstants.APP_NAME, AppConstants.VERSION_CODE).startXmppChat(HomeActivity.this);

        updateTreatment();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.img_home) {

            switchToFragment(new HomeFragmentNew(), FragmentConstants.HOMEFRAGMENT);
        }
        if (item.getItemId() == R.id.img_news) {
            Fragment fragment = new NaviaContentActivity();
            switchToFragment(fragment, FragmentConstants.CONTENTFRAGMENT);
        }
        if (item.getItemId() == R.id.img_profile) {
            Fragment fragment = new UserProfileFragment();
            switchToFragment(fragment, FragmentConstants.USERPROFILEFRAGMENT);
        }
        return true;
    }

    void switchToFragment(Fragment fragment, String Tag) {
        Fragment fragmentToAdd = fragment;
        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.placeholder, fragmentToAdd, Tag)
                .commit();


    }

    private void registerForFCM() {
        String token = MyPrefs.getString(this, FirebaseConstants.PREF_FIREBASE_TOKEN, "");

        if (token == null) {
            return;
        }

        String number = MyPrefs.getString(this, Constants.PHONE_ID, "");

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(number);
        stringBuilder.append("@");
        stringBuilder.append(AppConstants.APP_NAME);
        stringBuilder.append("/");
        stringBuilder.append("android");
        stringBuilder.append("/");
        stringBuilder.append("patient");

        Log.v("TOKENAA", token);

        registerGCM(stringBuilder.toString(), token, "fcm", number, AppConstants.APP_NAME);

    }

    public void registerGCM(String dev_id, String token, String register_type, String number, String appName) {
        final String url = Constants.URL.replace("/navia/", "") + "/messaging/gcm_register";
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dev_id", dev_id);
            jsonObject.put("reg_id", token);
            jsonObject.put("reg_type", register_type);
            jsonObject.put("name", MyPrefs.getString(MyApplication.getContext(), Constants.PREF_FIRST_NAME, ""));
            jsonObject.put("username", MyPrefs.getString(MyApplication.getContext(), Constants.PHONE_ID, ""));
            jsonObject.put("device_id", MyPrefs.getString(MyApplication.getContext(), Constants.PREF_PIN, ""));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("test", jsonObject.toString());

        Observable.defer(new Callable<ObservableSource<String>>() {
            @Override
            public ObservableSource<String> call() throws Exception {
                return Observable.just(OkhttpClientManager.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).postRequest(url,jsonObject.toString()));
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) { }

                    @Override
                    public void onNext(String s){ }

                    @Override
                    public void onError(Throwable e) { }

                    @Override
                    public void onComplete() { }
                });
    }


    public void StartSchedule(Context context) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        int interval = 24 * 60 * 60 * 1000;
        //int interval = 10*60*1000;
        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                interval, pendingIntent);
        //Toast.makeText(this, "Alarm Set", Toast.LENGTH_LONG).show();
    }



    private void initialiseHomeFragment() {
        // Create headingheader_box new fragment and specify the planet to show based on position
         fragment= new HomeFragmentNew();
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(R.id.placeholder, fragment,FragmentConstants.HOMEFRAGMENT)
                .commit();
        //  mDrawerLayout.closeDrawer(Gravity.LEFT);

    }

    private void showFragment(Fragment fragment, String tag) {
        // Insert the fragment by replacing any existing fragment
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(R.id.placeholder, fragment, tag)
                .addToBackStack(fragment.getClass().getName())
                .commit();
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    private void updateTreatment() {
        boolean careTreatment = false;
        UpdateHelper helper = new UpdateHelper(HomeActivity.this, careTreatment, false);
        TreatmentModuleDelegate.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE);
        helper.UpdateMyTreatments();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //   mDrawerToggle.syncState();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mBroadcastreceiver);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //  mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.info){
            Intent intent = new Intent(contex,DoctorProfileActivity.class);
            startActivity(intent);
        }


        else if (item.getItemId() == R.id.logout) {

            MyPrefs.putBoolean(this, Constants.PREF_REGISTERED, false);

            Utility.logoutTasks(this);
            finish();

            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void logOutTasks() {
        ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Deleting all data for user");
        progressDialog.show();
        // Perform logout tasks
        MyPrefs.clear(MyApplication.getContext());
        MyPrefs.putBoolean(MyApplication.getContext(), Constants.OPENED_ONCE, true);
        MyPrefs.putBoolean(this, Constants.PREF_REGISTERED, false);
        int currentVersionCode = 0;
        try {
            currentVersionCode = getPackageManager().
                    getPackageInfo(getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            // handle exception
            e.printStackTrace();
            return;
        }
        MyPrefs.putInt(MyApplication.getContext(), Constants.VERSION_CODE_KEY, currentVersionCode);
        deleteTreatment();

//        stopService(new Intent(this, XmppMessagingReceivingService.class));
        XmppChatDelegate.getInstance(AppConstants.APP_NAME, AppConstants.VERSION_CODE)
                .stopXmppChat(HomeActivity.this);
        // Dismiss the progress dialog
        progressDialog.dismiss();
    }

    private void deleteTreatment() {
        TasksDataSource db = TasksDataSource.getInstance(this); //get access to the instance of TasksDataSource
        // Delete all treatments
        db.deleteAllTasks();
        //Delete all follow ups
        db.deleteAllFolUpTasks();
        //Delete all side effects
        db.deleteAllSideEffects();
        db.deleteAllParameters();
    }


    @Override
    public void onResume() {
        super.onResume();

        // Close Drawer
        if (drawerlayout != null)
            drawerlayout.closeDrawer(Gravity.LEFT);

        String previouslyEncodedImage = MyPrefs.getString(this, Constants.PREF_IMAGE_URL, "");

        if (!previouslyEncodedImage.equalsIgnoreCase("")) {
            byte[] b = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
            ((ImageView) findViewById(R.id.userimage)).setImageBitmap(bitmap);
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastreceiver,
                new IntentFilter(GCMIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastreceiver,
                new IntentFilter(GCMIntentService.REGISTRATION_ERROR));

    }

    @Override
    public void onBackPressed() {
        final FragmentManager fm = getSupportFragmentManager();

        if (getSupportFragmentManager().findFragmentByTag(FragmentConstants.HOMEFRAGMENT) != null
                && getSupportFragmentManager().findFragmentByTag(FragmentConstants.HOMEFRAGMENT).isVisible()) {
            if (doubleBackToExitPressedOnce) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit the app", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else if (getSupportFragmentManager().findFragmentByTag(Constants.MyTreatment) != null
                && getSupportFragmentManager().findFragmentByTag(Constants.MyTreatment).isVisible()) {

            if (doubleBackToExitPressedOnce) {

                fragment = new HomeFragmentNew();

                FragmentManager fm1 = getSupportFragmentManager();
                for (int i = 0; i < fm1.getBackStackEntryCount(); ++i) {
                    fm1.popBackStack();
                }
                FragmentManager fragmentManager = getSupportFragmentManager();

                fragmentManager.beginTransaction()
                        .replace(R.id.placeholder, fragment, FragmentConstants.HOMEFRAGMENT)
                        .commit();
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else if (fm.getBackStackEntryCount() == 0) {
            fragment = new HomeFragmentNew();

            FragmentManager fm1 = getSupportFragmentManager();
            for (int i = 0; i < fm1.getBackStackEntryCount(); ++i) {
                fm1.popBackStack();
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.placeholder, fragment, FragmentConstants.HOMEFRAGMENT)
                    .commit();

        } else {
            super.onBackPressed();
            return;
        }
    }


    public class AsynctaskGCM extends AsyncTask<String, Void, JSONObject> {
        Activity mActivity;
        String namevaluepairs;

        public AsynctaskGCM(Activity activity, String nameValuePairs) {
            namevaluepairs = nameValuePairs;
            mActivity = activity;
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            String url = params[0];
            JSONObject response  = null;
            try {
                String s = OkhttpClientManager.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).postRequest(url,namevaluepairs);
                response=new JSONObject(s);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (response != null) {
                return response;
            } else {
                return null;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_home,menu);
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(disposable!=null)
        {
            disposable.dispose();
        }
    }
}
