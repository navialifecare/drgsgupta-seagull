package com.healthapp.drgsgupta;

public class AppConstants {
    public static final String APP_NAME = "GSGupta-Seagull";
    public static final String VERSION_CODE = "1";
    public static final String MEDICAL_DIRECTOR_PHONE_NUMBER = "9811070579";
    public static final String MEDICAL_DIRECTOR_NAME = "Dr GS Gupta";

    public static String HOSPITAL_NAME = "Diabetes, Hypertension & Heart Clinic";
    public static final String WEBSITE_URL="";
}
