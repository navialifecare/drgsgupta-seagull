package com.healthapp.drgsgupta;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.navia.naviahelp.HelperFunctions;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ContactUsActivity extends AppCompatActivity {

    public EditText EmailBody;
    Spinner SubjectSpinner;
    private String Subject_line = "General";
    Activity mActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_contact_us);

        EmailBody = (EditText) findViewById(R.id.Feedback_message);
        SubjectSpinner = (Spinner) findViewById(R.id.subject_line);
        final Button sendMail = (Button) findViewById(R.id.send_button);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        toolbar.setTitle("Contact Us");
        setSupportActionBar(toolbar);
        HelperFunctions.changeToolbarTitle(toolbar, FontNamesClass.fontBold);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        List<String> Subject = new ArrayList<String>();
        Subject.add("Feedback");
//        Subject.add("Careers");
        Subject.add("Issue Faced");
        Subject.add("General");

        mActivity = this;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, Subject);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SubjectSpinner.setAdapter(dataAdapter);


        SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Subject_line = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        sendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail(EmailBody.getText().toString());
            }
        });


    }

    public void sendEmail(String emailContent) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@navialifecare.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, Subject_line);
        i.putExtra(Intent.EXTRA_TEXT, emailContent);
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(mActivity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

}
