package com.healthapp.drgsgupta;

public interface ResponseCallback {
    void onApiCallSuccess(Object responseObject);

    void onAuthTokenRefreshRequired();

    void onApiCallFailure(String errorReason);
}