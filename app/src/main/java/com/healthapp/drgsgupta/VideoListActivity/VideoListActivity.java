package com.healthapp.drgsgupta.VideoListActivity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.healthapp.drgsgupta.NaviaContentActivity.VideoItem;
import com.healthapp.drgsgupta.R;
import com.navia.naviahelp.HelperFunctions;

import java.util.ArrayList;


/**
 * Created by amitgupta on 12/4/17.
 */

public class VideoListActivity extends AppCompatActivity implements VideoListActivityView {
    Activity mActivity;
    RecyclerView mRecyclerView;
    TextView tv_empty;
    VideoListActivityPresenter mPresenter;
    ProgressBar progressBar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video_list);

        mActivity = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.app_bar);
        toolbar.setTitle("Videos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mRecyclerView =(RecyclerView)findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        tv_empty = (TextView)findViewById(R.id.tv_empty);

        mPresenter = new VideoListActivityPresenter(this);

        getVideos();

    }


    void getVideos(){
      mPresenter.loadVideos();
    }

    @Override
    public void loadVideoProgressShow() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void loadVideoProgressHide() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showList(ArrayList<VideoItem> arrayList) {
        VideoListAdapter adapter = new VideoListAdapter(mActivity,arrayList,mRecyclerView);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void showError() {
//        HelperFunctions.showApplicationToast(getString(R.string.SomethingWentWrong));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return true;
    }


}
