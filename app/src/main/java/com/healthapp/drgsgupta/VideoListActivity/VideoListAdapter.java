package com.healthapp.drgsgupta.VideoListActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthapp.drgsgupta.FontNamesClass;
import com.healthapp.drgsgupta.NaviaContentActivity.VideoItem;
import com.healthapp.drgsgupta.NaviaContentActivity.YoutubeActivity;
import com.healthapp.drgsgupta.R;
import com.navia.naviahelp.BundleConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


/**
 * Created by amitgupta on 12/4/17.
 */

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.MyViewHolder> {

    Activity mContext;
    LayoutInflater layoutInflater;
    ArrayList<VideoItem> mVideosList;
    String API_KEY="AIzaSyCNqFywtid1yIujn_Lv4TSZGNcVwSAkCPI";
    Typeface typeface;
    RecyclerView mRecyclerView;

    public VideoListAdapter(Activity context, ArrayList<VideoItem> list, RecyclerView recyclerView) {
        mContext = context;
        layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mVideosList=list;
        typeface = Typeface.createFromAsset(context.getAssets(), FontNamesClass.fontSemibold);
        mRecyclerView =recyclerView;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_video1,parent,false);
        view.setOnClickListener(new MyOnClickListener());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String url = mVideosList.get(position).getVideoUrl();
        holder.tv_title.setText(mVideosList.get(position).getTitle());
        holder.tv_title.setTypeface(typeface);
        AsyncTaskInfo asyncTaskInfo = new AsyncTaskInfo(holder.tv_title,holder.img_video);
        asyncTaskInfo.execute("https://www.googleapis.com/youtube/v3/videos?key="+API_KEY+"&part=snippet&id="+url);


    }

    @Override
    public int getItemCount() {
        return mVideosList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tv_title;
        ImageView img_video;
        public MyViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView)itemView.findViewById(R.id.tv_title);
            img_video = (ImageView)itemView.findViewById(R.id.img_video);
        }
    }

    class AsyncTaskInfo extends AsyncTask<String,Void,String> {

        TextView tv_final;
        ImageView img_final;

        public AsyncTaskInfo(TextView tv,ImageView img) {
            img_final= img;
            tv_final=tv;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL URL = new URL(params[0]);
                HttpURLConnection httpURLConnection =(HttpURLConnection)URL.openConnection();
                httpURLConnection.setConnectTimeout(20000);
                httpURLConnection.setReadTimeout(20000);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line="";
                while ((line=bufferedReader.readLine())!=null){
                    stringBuilder.append(line);
                }
                bufferedReader.close();
                return stringBuilder.toString();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray items = jsonObject.getJSONArray("items");
                JSONObject json_item = items.getJSONObject(0);
                JSONObject snippet = json_item.getJSONObject("snippet");
                String title = snippet.getString("title");
                JSONObject thumbnails = snippet.getJSONObject("thumbnails");
                JSONObject default_img = thumbnails.getJSONObject("medium");
                String img_url = default_img.getString("url");
                Picasso.get().load(img_url).error(R.drawable.ic_video_placeholder).
                        placeholder(R.drawable.ic_video_placeholder).into(img_final);
                tv_final.setText(title);

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    class MyOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            int pos = mRecyclerView.getChildLayoutPosition(v);
            Intent intent = new Intent(mContext, YoutubeActivity.class);
            intent.putExtra(BundleConstants.BUNDLE_VIDEO_URL,mVideosList.get(pos).getVideoUrl().split("v=")[1].substring(0,11));
            mContext.startActivity(intent);
        }
    }

}

