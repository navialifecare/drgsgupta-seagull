package com.healthapp.drgsgupta.VideoListActivity;

/**
 * Created by amitgupta on 12/5/17.
 */

public interface VideoListPresenterContract {

    void loadVideos();

}
