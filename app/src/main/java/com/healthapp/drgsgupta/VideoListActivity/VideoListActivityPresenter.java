package com.healthapp.drgsgupta.VideoListActivity;


import com.healthapp.drgsgupta.NaviaContentActivity.FetchContentModel;
import com.healthapp.drgsgupta.NaviaContentActivity.VideoFetchCallBack;
import com.healthapp.drgsgupta.NaviaContentActivity.VideoItem;

import java.util.ArrayList;


/**
 * Created by amitgupta on 12/5/17.
 */

public class VideoListActivityPresenter  implements VideoListPresenterContract,VideoFetchCallBack {

    VideoListActivityView mView;

    public VideoListActivityPresenter(VideoListActivityView view) {
        mView = view;
    }

    @Override
    public void loadVideos() {
        mView.loadVideoProgressShow();
        FetchContentModel contentModelRep = new FetchContentModel(this);
        contentModelRep.getVideos();
    }

    @Override
    public void returnVideos(ArrayList<VideoItem> videos) {
        mView.loadVideoProgressHide();
        mView.showList(videos);
    }

    @Override
    public void onError(Object error) {
        mView.showError();
    }
}
