package com.healthapp.drgsgupta.VideoListActivity;


import com.healthapp.drgsgupta.NaviaContentActivity.VideoItem;

import java.util.ArrayList;


/**
 * Created by amitgupta on 12/5/17.
 */

public interface VideoListActivityView  {

    void loadVideoProgressShow();
    void loadVideoProgressHide();
    void showList(ArrayList<VideoItem> arrayList);
    void showError();
}
