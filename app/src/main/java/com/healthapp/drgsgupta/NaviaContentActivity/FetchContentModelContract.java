package com.healthapp.drgsgupta.NaviaContentActivity;

/**
 * Created by amitgupta on 12/14/17.
 */

public interface FetchContentModelContract {

    void fetchContent(String authToken, String phoneNumber);

    void getVideos();
    void getBlogs();
}
