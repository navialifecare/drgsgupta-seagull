package com.healthapp.drgsgupta.NaviaContentActivity;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;

import java.util.ArrayList;

/**
 * Created by amitgupta on 12/13/17.
 */

public class ViewPagerTip extends PagerAdapter {
    Context mContext;
    ArrayList<ArticleItem> mArraylist;
    LayoutInflater mLayoutInflater;

    public ViewPagerTip(Context context, ArrayList<ArticleItem> arrayList) {
        mContext = context;
        mArraylist = arrayList;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_tip_of_the_day,container,false);

        TextView tv_title = (TextView)itemView.findViewById(R.id.tv_title);
        TextView tv_content = (TextView) itemView.findViewById(R.id.tv_content);

        tv_title.setText(mArraylist.get(position).getHeading());
        tv_content.setText(mArraylist.get(position).getContent());

        container.addView(itemView);

        return itemView;

    }

    @Override
    public int getCount() {
        return mArraylist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object==view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }
}
