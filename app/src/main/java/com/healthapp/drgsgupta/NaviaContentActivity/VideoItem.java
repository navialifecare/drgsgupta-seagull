package com.healthapp.drgsgupta.NaviaContentActivity;

import java.io.Serializable;

/**
 * Created by amitgupta on 12/13/17.
 */

public class VideoItem implements Serializable {
    String videoUrl;
    String imgUrl;
    String title;

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
