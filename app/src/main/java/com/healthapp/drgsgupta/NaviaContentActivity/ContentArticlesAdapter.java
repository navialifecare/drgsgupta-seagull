package com.healthapp.drgsgupta.NaviaContentActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthapp.drgsgupta.FontNamesClass;
import com.healthapp.drgsgupta.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by amitgupta on 6/1/17.
 */

public class ContentArticlesAdapter extends RecyclerView.Adapter<ContentArticlesAdapter.MyViewHolder> {

    ArrayList<ArticleItem> mArrayList;
    Activity mContext;
    LayoutInflater layoutInflater;
    RecyclerView mRecyclerView;
    boolean isHorizontalLayout;
    Typeface typeface;



    public ContentArticlesAdapter(ArrayList<ArticleItem> mArrayList, Activity mContext, RecyclerView recyclerview, boolean isHorizontal) {
        this.mArrayList = mArrayList;
        this.mContext = mContext;
        layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRecyclerView = recyclerview;
        isHorizontalLayout = isHorizontal;
        typeface = Typeface.createFromAsset(mContext.getAssets(), FontNamesClass.fontSemibold);
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        //View rootview = recyclerView.getRootView();
        //rootview.getBackground().setColorFilter(mContext.getResources().getColor(R.color.doctor_app_color), PorterDuff.Mode.DARKEN);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
            view = layoutInflater.inflate(R.layout.item_blog,parent,false);
       view.setOnClickListener(new ArticleOnClickListener());
      return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       holder.tv_content_heading.setText(mArrayList.get(position).getHeading());
        holder.tv_content_heading.setTypeface(typeface);
       try {
           Picasso.get().load(mArrayList.get(position).getImg_url()).error(R.drawable.bg_img_placeholder).
                   placeholder(R.drawable.bg_img_placeholder).into(holder.img_content);
       }catch (IllegalArgumentException e){
           e.printStackTrace();
           holder.img_content.setImageResource(R.drawable.bg_img_placeholder);
       }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView img_content;
        TextView tv_content_heading;

        public MyViewHolder(View itemView) {
            super(itemView);
             img_content = (ImageView)itemView.findViewById(R.id.img_content);
             tv_content_heading =  (TextView)itemView.findViewById(R.id.tv_content);
        }
    }

    class ArticleOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            int pos = mRecyclerView.getChildLayoutPosition(v);
            Intent intent = new Intent(mContext,ContentActivity.class);
            intent.putExtra("title_article",mArrayList.get(pos).getHeading());
            intent.putExtra("id_article",mArrayList.get(pos).getId());
            intent.putExtra("content_article",mArrayList.get(pos).getContent());
            intent.putExtra("img_article",mArrayList.get(pos).getImg_url());
            intent.putExtra("date_article",mArrayList.get(pos).getDate());
            if(Build.VERSION.SDK_INT>=21){
                ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(mContext,v.findViewById(R.id.img_content),mContext.getResources().
                                getString(R.string.image_transition));
             mContext.startActivity(intent,activityOptionsCompat.toBundle());
            }
            else {
                mContext.startActivity(intent);
            }
        }
    }
}
