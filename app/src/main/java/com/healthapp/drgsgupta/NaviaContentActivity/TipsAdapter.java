package com.healthapp.drgsgupta.NaviaContentActivity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;

import java.util.ArrayList;

/**
 * Created by amitgupta on 12/25/17.
 */


public class TipsAdapter extends RecyclerView.Adapter<TipsAdapter.MyViewHolder> {

    ArrayList<ArticleItem> mArrayList;
    Activity mContext;
    LayoutInflater layoutInflater;
    RecyclerView mRecyclerView;
    boolean isHorizontalLayout;
    Typeface typeface;



    public TipsAdapter(Activity mContext, ArrayList<ArticleItem> mArrayList) {
        this.mArrayList = mArrayList;
        this.mContext = mContext;
        layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
            view = layoutInflater.inflate(R.layout.item_tip_of_the_day, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tv_title.setText(mArrayList.get(position).getHeading());
        holder.tv_content.setText(mArrayList.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tv_title,tv_content;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_content=  (TextView)itemView.findViewById(R.id.tv_content);
        }
    }


}