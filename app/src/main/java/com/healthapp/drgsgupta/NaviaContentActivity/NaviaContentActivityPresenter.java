package com.healthapp.drgsgupta.NaviaContentActivity;


import java.util.ArrayList;

/**
 * Created by amitgupta on 12/14/17.
 */

public class NaviaContentActivityPresenter  implements AllContentPresenterContract ,ContentFetchCallBack{

    private AllContentActivityView mView;
    private FetchContentModel fetchContentModel;

    public NaviaContentActivityPresenter(AllContentActivityView view) {
        mView = view;
        fetchContentModel = new FetchContentModel(this);
    }

    @Override
    public void loadBlogs(String authToken, String phoneNumber) {
        mView.showProgress();
        fetchContentModel.fetchContent(authToken, phoneNumber);
    }

    @Override
    public void onVideoClicked(VideoItem videoItem) {
        mView.openVideo(videoItem);
    }

    @Override
    public void onBlogClicked(ArticleItem articleItem) {
         mView.openBlog(articleItem);
    }

    @Override
    public void onViewAllBlogsClicked() {
     mView.openContentActivity();
    }

    @Override
    public void onViewAllVideosClicked() {
      mView.openVideosActivity();
    }


    @Override
    public void returnBlogs(ArrayList<ArticleItem> blogs) {
        mView.hideProgress();
        if(blogs.size()==0){
            mView.showEmptyBlogs();
            return;
        }
        mView.showBlogs(blogs);

    }

    @Override
    public void returnVideos(ArrayList<VideoItem> videos) {
        if(videos.size()==0){
            mView.showEmptyVideos();
            return;
        }

        mView.showVideos(videos);
    }

    @Override
    public void returnTips(ArrayList<ArticleItem> tips) {
        if(tips.size()==0){
            mView.showEmptyTips();
            return;
        }
        mView.showTips(tips);
    }

    @Override
    public void onFailure(Object object) {
      mView.hideProgress();
//      if(object instanceof NoConnectionError){
//          mView.showNoInternetConnectionError();
//      }
       if(object instanceof String){
          mView.showErrorString((String) object);
      }
      else {
          mView.showError();
      }
    }

    @Override
    public void onViewAllTipsClicked() {
        mView.openTipsActivity();
    }
}
