package com.healthapp.drgsgupta.NaviaContentActivity;



import java.util.ArrayList;

/**
 * Created by amitgupta on 12/14/17.
 */

public interface AllContentActivityView {

    void showProgress();
    void hideProgress();
    void showError();
    void showErrorString(String error);
    void showVideos(ArrayList<VideoItem> arrayList);
    void showBlogs(ArrayList<ArticleItem> articleItems);
    void showTips(ArrayList<ArticleItem> tips);
    void openBlog(ArticleItem articleItem);
    void openVideo(VideoItem videoItem);
    void openVideosActivity();
    void openContentActivity();
    void openTipsActivity();
    void showEmptyBlogs();
    void showEmptyVideos();
    void showEmptyTips();
    void showNoInternetConnectionError();

}
