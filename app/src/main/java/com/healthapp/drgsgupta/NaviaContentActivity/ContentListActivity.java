package com.healthapp.drgsgupta.NaviaContentActivity;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.healthapp.drgsgupta.FontNamesClass;
import com.healthapp.drgsgupta.R;
import com.navia.naviahelp.HelperFunctions;

import java.util.ArrayList;

/**
 * Created by amitgupta on 6/5/17.
 */

public class ContentListActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    Activity mActivity;
    ProgressBar progressBar_content;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_list_activity);

        mActivity= this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.app_bar);
        toolbar.setTitle("Articles");

        toolbar.setBackgroundColor(getColor(R.color.doctor_app_color));
        toolbar.setTitleTextColor(getColor(R.color.white));
        setSupportActionBar(toolbar);
        HelperFunctions.changeToolbarTitle(toolbar,FontNamesClass.fontBold);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar_content = (ProgressBar)findViewById(R.id.progressBar);

        mRecyclerView =(RecyclerView)findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        getContentFromCache();

    }

    void getContentFromCache(){
        progressBar_content.setVisibility(View.INVISIBLE);
        final ArrayList<ArticleItem> articleItems = ContentCache.getArrayListContent();

        ContentArticlesAdapter contentArticlesAdapter = new ContentArticlesAdapter(articleItems,mActivity,mRecyclerView,false);
        mRecyclerView.setAdapter(contentArticlesAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home : finish();
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }


}
