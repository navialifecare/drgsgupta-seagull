package com.healthapp.drgsgupta.NaviaContentActivity;

import java.util.ArrayList;

/**
 * Created by amitgupta on 1/3/18.
 */

public interface VideoFetchCallBack {

    void returnVideos(ArrayList<VideoItem> videos);
    void onError(Object error);
}
