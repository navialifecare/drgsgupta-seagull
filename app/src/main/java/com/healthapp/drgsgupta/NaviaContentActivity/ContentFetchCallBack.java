package com.healthapp.drgsgupta.NaviaContentActivity;

import java.util.ArrayList;

/**
 * Created by amitgupta on 1/3/18.
 */

public interface ContentFetchCallBack {

    void returnBlogs(ArrayList<ArticleItem> blogs);
    void returnVideos(ArrayList<VideoItem> videos);
    void returnTips(ArrayList<ArticleItem> tips);
    void onFailure(Object o);
}
