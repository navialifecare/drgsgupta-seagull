package com.healthapp.drgsgupta.NaviaContentActivity;

import android.content.Context;

import com.healthapp.drgsgupta.MyApplication;
import com.healthapp.drgsgupta.ResponseCallback;
import com.healthapp.drgsgupta.Utility;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by amitgupta on 12/14/17.
 */

public class FetchContentModel implements FetchContentModelContract {

    ContentFetchCallBack mContentFetchCallBack;
    VideoFetchCallBack mVideoFetchCallBack;
    String video_file_name = "videos_";
    String content_file_name = "content_";

    public FetchContentModel(ContentFetchCallBack callback) {
        mContentFetchCallBack = callback;
    }

    public FetchContentModel(VideoFetchCallBack callBack) {
        mVideoFetchCallBack = callBack;
    }


    @Override
    public void fetchContent(final String authToken, final String phoneNumber) {
        final ArrayList<VideoItem> arrayListVideos = new ArrayList<>();
        final ArrayList<ArticleItem> arrayListBlogs = new ArrayList<>();
        final ArrayList<ArticleItem> arrayListTips = new ArrayList<>();

        //If present in cache load from there
        if (ContentCache.getArrayListContent() != null) {
            mContentFetchCallBack.returnVideos(ContentCache.getArrayListVideos());
            mContentFetchCallBack.returnBlogs(ContentCache.getArrayListContent());
            mContentFetchCallBack.returnTips(ContentCache.getArrayListTips());
            return;
        }




        ContentApiManager.getInstance().fetchContent(phoneNumber, authToken, new ResponseCallback() {
            @Override
            public void onApiCallSuccess(Object responseObject) {
                if (responseObject instanceof ContentResponse) {
                    if (((ContentResponse) responseObject).getResult().equals("SUCCESS")) {
                        List<ContentResponse.Content> contents = ((ContentResponse) responseObject).getContent();
                        if(contents != null) {
                            for (ContentResponse.Content content : contents) {
                                if (content.getContentType().equals("Video")) {
                                    VideoItem videoItem = new VideoItem();

                                    if (content.getImage() != null)
                                        videoItem.setImgUrl(content.getImage());
                                    else videoItem.setImgUrl(content.getImageLink());

                                    videoItem.setTitle(content.getTitle());

                                    if (content.getFileLink() != null)
                                        videoItem.setVideoUrl(content.getFileLink());
                                    else videoItem.setVideoUrl(content.getFile());

                                    arrayListVideos.add(videoItem);

                                } else if (content.getContentType().equals("Tips")) {
                                    ArticleItem articleItem = new ArticleItem();
                                    articleItem.setHeading(content.getTitle());

                                    if (content.getImage() != null)
                                        articleItem.setImg_url(content.getImage());
                                    else articleItem.setImg_url(content.getImageLink());

                                    articleItem.setContent(content.getContent());

                                    articleItem.setDate(content.getCreationDate());

                                    arrayListTips.add(articleItem);
                                } else {
                                    //Content
                                    ArticleItem articleItem = new ArticleItem();
                                    articleItem.setHeading(content.getTitle());

                                    if (content.getImage() != null)
                                        articleItem.setImg_url(content.getImage());
                                    else articleItem.setImg_url(content.getImageLink());

                                    articleItem.setContent(content.getContent());

                                    articleItem.setDate(content.getCreationDate());

                                    arrayListBlogs.add(articleItem);
                                }
                            }
                            mContentFetchCallBack.returnVideos(arrayListVideos);
                            mContentFetchCallBack.returnBlogs(arrayListBlogs);
                            mContentFetchCallBack.returnTips(arrayListTips);

                            ContentCache.setArrayListContent(arrayListBlogs);
                            ContentCache.setArrayListTips(arrayListTips);
                            ContentCache.setArrayListVideos(arrayListVideos);
                        } else {
                            //show empty)
                            mContentFetchCallBack.returnVideos(arrayListVideos);
                            mContentFetchCallBack.returnBlogs(arrayListBlogs);
                            mContentFetchCallBack.returnTips(arrayListTips);

                        }
                    }
                }
            }

            @Override
            public void onAuthTokenRefreshRequired() { }

            @Override
            public void onApiCallFailure(String errorReason) {
                mContentFetchCallBack.onFailure(errorReason);
            }
        });

    }


    @Override
    public void getVideos() {
        final ArrayList<VideoItem> videoList = ContentCache.getArrayListVideos();
        mVideoFetchCallBack.returnVideos(videoList);
    }

    @Override
    public void getBlogs() {
    }

}
