package com.healthapp.drgsgupta.NaviaContentActivity;

import java.util.ArrayList;

/**
 * Created by amitgupta on 12/22/17.
 */

public class ContentCache {

    //Caches content till the application is not closed.

    public static ArrayList<VideoItem> arrayListVideos;
    public static ArrayList<ArticleItem> arrayListContent;
    public static ArrayList<ArticleItem> arrayListTips;

    public static ArrayList<VideoItem> getArrayListVideos() {
        return arrayListVideos;
    }

    public static void setArrayListVideos(ArrayList<VideoItem> arrayListVideos) {
        ContentCache.arrayListVideos = arrayListVideos;
    }

    public static ArrayList<ArticleItem> getArrayListContent() {
        return arrayListContent;
    }

    public static void setArrayListContent(ArrayList<ArticleItem> arrayListContent) {
        ContentCache.arrayListContent = arrayListContent;
    }

    public static ArrayList<ArticleItem> getArrayListTips() {
        return arrayListTips;
    }

    public static void setArrayListTips(ArrayList<ArticleItem> arrayListTips) {
        ContentCache.arrayListTips = arrayListTips;
    }
}
