package com.healthapp.drgsgupta.NaviaContentActivity;


import com.navia.naviahelp.HelperFunctions;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by amitgupta on 6/1/17.
 */

public class ArticleItem implements Comparator<String>,Serializable {
    String heading;
    String img_url;
    String content;
    String date;
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    @Override
    public int compare(String o1, String o2) {
        Long date1 = HelperFunctions.convertDateStringTolong(o1,6);
        Long date2 = HelperFunctions.convertDateStringTolong(o2,6);
        return date2.compareTo(date1);
    }
}
