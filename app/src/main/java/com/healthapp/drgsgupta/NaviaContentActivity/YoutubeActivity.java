package com.healthapp.drgsgupta.NaviaContentActivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.healthapp.drgsgupta.R;
import com.navia.naviahelp.BundleConstants;

/**
 * Created by amitgupta on 7/7/17.
 */

public class YoutubeActivity extends YouTubeBaseActivity {
    String API_KEY="AIzaSyB5d-r-NfYhxL4oFrc9DDSXeRgXZRr8Gu4";
    AlertDialog alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube);

        final String VIDEO_URL = getIntent().getStringExtra(BundleConstants.BUNDLE_VIDEO_URL);


        YouTubePlayerView youTubePlayerView = (YouTubePlayerView)findViewById(R.id.youtube_view);
        youTubePlayerView.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(VIDEO_URL);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Toast.makeText(YoutubeActivity.this,"Video could not be loaded,Try again",Toast.LENGTH_SHORT).show();
            }
        });
    }




}
