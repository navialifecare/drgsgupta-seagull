package com.healthapp.drgsgupta.NaviaContentActivity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;

import java.util.ArrayList;

/**
 * Created by amitgupta on 12/26/17.
 */

public class TipsActivity extends AppCompatActivity {
    Activity mActivity;
    RecyclerView mRecyclerView;
    TextView tv_empty;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);

        mActivity = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.app_bar);
        toolbar.setTitle("Tips");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        tv_empty = (TextView)findViewById(R.id.tv_empty);

        getTips();
    }


    void getTips(){
        ArrayList<ArticleItem> tipsArrayList = ContentCache.getArrayListTips();
        if(tipsArrayList!=null){
            if(tipsArrayList.size()>0){
                TipsAdapter tipsAdapter = new TipsAdapter(mActivity,tipsArrayList);
                mRecyclerView.setAdapter(tipsAdapter);
            }
            else{
                tv_empty.setVisibility(View.VISIBLE);
            }
        }
        else{
            tv_empty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return true;
    }
}
