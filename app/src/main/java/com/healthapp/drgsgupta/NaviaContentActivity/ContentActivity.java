package com.healthapp.drgsgupta.NaviaContentActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.healthapp.drgsgupta.AppConstants;
import com.healthapp.drgsgupta.FontNamesClass;
import com.healthapp.drgsgupta.R;
import com.navia.naviahelp.BundleConstants;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.HelperFunctions;
import com.navia.naviahelp.OkhttpClientManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by amitgupta on 6/1/17.
 */

public class ContentActivity extends AppCompatActivity {
    Activity mActivity;
    ProgressBar progressBar;
    TextView tv_article_content;
    Disposable disposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_detail);
        mActivity= this;


        Bundle bundle = getIntent().getExtras();

        Toolbar toolbar = (Toolbar)findViewById(R.id.app_bar);
        try {
            toolbar.setTitle(bundle.getString("title_article"));
            toolbar.setBackgroundColor(getColor(R.color.doctor_app_color));
            toolbar.setTitleTextColor(getColor(R.color.white));
        }catch (Exception e){
            e.printStackTrace();
        }

        setSupportActionBar(toolbar);

        HelperFunctions.changeToolbarTitle(toolbar,FontNamesClass.fontBold);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Typeface typeface = Typeface.createFromAsset(getAssets(), FontNamesClass.fontSemibold);
        Typeface regular = Typeface.createFromAsset(getAssets(),FontNamesClass.fontRegular);

        final String img_url = bundle.getString(BundleConstants.BUNDLE_IMAGE_ARTICLE);
        String article_title  = bundle.getString(BundleConstants.BUNDLE_TITLE_ARTICLE);
        String article_content = bundle.getString(BundleConstants.BUNDLE_CONTENT_ARTICLE);
        String article_id = bundle.getString(BundleConstants.BUNDLE_ID_ARTICLE);



        TextView tv_article_title = (TextView)findViewById(R.id.tv_article_title);
        tv_article_content = (TextView)findViewById(R.id.tv_article_content);
        ImageView img_article = (ImageView)findViewById(R.id.img_article);
        TextView tv_article_date = (TextView)findViewById(R.id.tv_date);
        progressBar = (ProgressBar)findViewById(R.id.progressBar) ;

        tv_article_content.setTypeface(regular);
        tv_article_title.setTypeface(typeface);
        tv_article_date.setTypeface(typeface);

        tv_article_title.setText(article_title);
        tv_article_content.setText(article_content);
        Picasso.get().load(img_url).error(R.drawable.bg_img_placeholder).
                placeholder(R.drawable.bg_img_placeholder).into(img_article);
        img_article.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent= new Intent(ContentActivity.this,ImageOpenActivity.class);
                intent.putExtra("image_url",img_url);
                startActivity(intent);
            }
        });

        if(article_content.trim().length()<3){
            String id = article_id;
            String content= getContentFromBackEnd(id);
        }


    }

    String getContentFromBackEnd(String id){

        // since content can't be pushed through gcm since its so long we make an api call to fetch content

        final StringBuilder content_string = new StringBuilder();
        String URL = Constants.URL.replace("/navia/","");
        //get request filter
        URL    = URL  +"/content/get/content?id="+id;

        final String finalURL = URL;
        Observable.defer(new Callable<ObservableSource<String>>() {
            @Override
            public ObservableSource<String> call() throws Exception {
                return Observable.just(OkhttpClientManager.getInstance(AppConstants.APP_NAME,AppConstants.VERSION_CODE).getRequest(finalURL));
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable=d;
                    }

                    @Override
                    public void onNext(String s){
                        try {
                            JSONObject response=new JSONObject(s);
                            if (response.getString("result").equals("SUCCESS")) {
                                JSONArray content_array = response.getJSONArray("content");
                                JSONObject content_item_json = content_array.getJSONObject(0);
                                content_string .append(content_item_json.getString("content"));
                                tv_article_content.setText(content_string);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) { }

                    @Override
                    public void onComplete() { }
                });

        return content_string.toString();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home : finish();
                return true;
                default: return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(disposable!=null){
            disposable.dispose();
        }
    }
}

