package com.healthapp.drgsgupta.NaviaContentActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;
import com.navia.naviahelp.BundleConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by amitgupta on 12/13/17.
 */

public class ViewPagerAdapterVideo extends PagerAdapter {
    Context mContext;
    ArrayList<VideoItem> mArraylist;
    LayoutInflater mLayoutInflater;

    public ViewPagerAdapterVideo(Context context, ArrayList<VideoItem> arrayList) {
        mContext = context;
        mArraylist = arrayList;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mArraylist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_video1,container,false);

        TextView tv_title = (TextView)itemView.findViewById(R.id.tv_title);
        ImageView img_video = (ImageView)itemView.findViewById(R.id.img_video);

        tv_title.setText(mArraylist.get(position).getTitle());
        try {
            Picasso.get().load(mArraylist.get(position).getImgUrl()).into(img_video);
        }catch (IllegalArgumentException e){
            Picasso.get().load(R.drawable.bg_img_placeholder).into(img_video);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,YoutubeActivity.class);
                intent.putExtra(BundleConstants.BUNDLE_VIDEO_URL,mArraylist.get(position).getVideoUrl().split("v=")[1].substring(0,11));
                mContext.startActivity(intent);

            }
        });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View)object);
    }
}
