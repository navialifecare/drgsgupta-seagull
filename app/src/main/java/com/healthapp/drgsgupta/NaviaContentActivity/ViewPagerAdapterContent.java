package com.healthapp.drgsgupta.NaviaContentActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;
import com.navia.naviahelp.BundleConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by amitgupta on 12/13/17.
 */

public class ViewPagerAdapterContent extends PagerAdapter {
    Context mContext;
    ArrayList<ArticleItem> mArraylist;
    LayoutInflater mLayoutInflater;
    ImageView indicators[]=new ImageView[3];

    public ViewPagerAdapterContent(Context context, ArrayList<ArticleItem> arrayList) {
        mContext = context;
        mArraylist = arrayList;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_blog,container,false);

        TextView tv_title = (TextView)itemView.findViewById(R.id.tv_content);
        ImageView img_video = (ImageView)itemView.findViewById(R.id.img_content);

        tv_title.setText(mArraylist.get(position).getHeading());
        try {
            Picasso.get().load(mArraylist.get(position).getImg_url()).into(img_video);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,ContentActivity.class);
                intent.putExtra(BundleConstants.BUNDLE_ID_ARTICLE,mArraylist.get(position).getId());
                intent.putExtra(BundleConstants.BUNDLE_IMAGE_ARTICLE,mArraylist.get(position).getImg_url());
                intent.putExtra(BundleConstants.BUNDLE_CONTENT_ARTICLE,mArraylist.get(position).getContent());
                intent.putExtra(BundleConstants.BUNDLE_DATE_ARTICLE,mArraylist.get(position).getDate());
                intent.putExtra(BundleConstants.BUNDLE_TITLE_ARTICLE,mArraylist.get(position).getHeading());
                mContext.startActivity(intent);
            }
        });

        container.addView(itemView);

        return itemView;

    }

    @Override
    public int getCount() {
        return mArraylist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object==view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }
}
