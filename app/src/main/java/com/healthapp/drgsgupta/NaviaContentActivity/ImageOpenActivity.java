package com.healthapp.drgsgupta.NaviaContentActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.healthapp.drgsgupta.R;
import com.squareup.picasso.Picasso;

/**
 * Created by amitgupta on 6/30/17.
 */

public class ImageOpenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_image_content);

        String img_url = getIntent().getStringExtra("image_url");

        ImageView img_article = (ImageView)findViewById(R.id.img_article);

        Toolbar toolbar = (Toolbar)findViewById(R.id.app_bar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Picasso.get().load(img_url).error(R.drawable.bg_img_placeholder).
                placeholder(R.drawable.bg_img_placeholder).into(img_article);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home :finish();
                break;
            default: return super.onOptionsItemSelected(item);
        }
        return true;
    }
}
