package com.healthapp.drgsgupta.NaviaContentActivity;


/**
 * Created by amitgupta on 12/14/17.
 */

public interface AllContentPresenterContract {

   void loadBlogs(String authToken, String phoneNumber);
   void onVideoClicked(VideoItem videoItem);
   void onBlogClicked(ArticleItem blogItem);
   void onViewAllBlogsClicked();
   void onViewAllVideosClicked();
   void onViewAllTipsClicked();
   void onFailure(Object object);
}
