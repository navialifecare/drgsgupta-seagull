package com.healthapp.drgsgupta.NaviaContentActivity;

import android.util.Log;

import com.google.gson.Gson;
import com.healthapp.drgsgupta.AppConstants;
import com.healthapp.drgsgupta.ResponseCallback;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.OkhttpClientManager;

import java.io.IOException;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Shikhar on 3/22/2018.
 */

public class ContentApiManager {
    private static ContentApiManager manager;
    private static OkHttpClient client = OkhttpClientManager.getClient();
    private static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    public static ContentApiManager getInstance() {
        if (manager == null)
            createInstance();
        return manager;
    }

    private static void createInstance() {
        if (manager == null) {
            synchronized (ContentApiManager.class) {
                if (manager == null) {
                    manager = new ContentApiManager();
                }
            }
        }
    }

    public void fetchContent(final String username, final String authToken, final ResponseCallback apiCallback) {
        Observer<ContentResponse> observer = new Observer<ContentResponse>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ContentResponse contentResponse) {
                switch (contentResponse.getResult()) {
                    case Constants.AUTH_TOKEN_REFRESH_REQUIRED:
                        apiCallback.onAuthTokenRefreshRequired();
                        break;
                    case "SUCCESS":
                        apiCallback.onApiCallSuccess(contentResponse);
                        break;
                    default:
                        apiCallback.onApiCallFailure(contentResponse.getError_reason());
                        break;
                }
            }

            @Override
            public void onError(Throwable e) {
                apiCallback.onApiCallFailure(e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };

        Observable.defer(new Callable<ObservableSource<ContentResponse>>() {
            @Override
            public ObservableSource<ContentResponse> call() throws Exception {
                return Observable.just(fetchHealthFeedContent());
            }

            private ContentResponse fetchHealthFeedContent() throws IOException {
                String url = Constants.NEW_TEST_URL + "content/?partner=" + AppConstants.APP_NAME;
//                String url = Constants.NEW_TEST_URL + "content/?partner=Radix";
                String contentResponse = getResponse(url, username, authToken, client);
                if (contentResponse.equals(Constants.AUTH_TOKEN_REFRESH_REQUIRED)) {
                    ContentResponse response = new ContentResponse();
                    response.setResult(contentResponse);
                    return response;
                } else
                    return new Gson().fromJson(contentResponse, ContentResponse.class);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }

    private String getResponse(String url, String username, String authToken, OkHttpClient client) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("x-appname", AppConstants.APP_NAME)
                .addHeader("x-apptype", Constants.ANDROID)
                .addHeader("x-appversion",AppConstants.VERSION_CODE)
                .addHeader("Authentication-Token", authToken)
                .addHeader("x-debug", String.valueOf(Constants.DEBUG_STATUS))
                .addHeader("x-username", username)
                .addHeader("x-usertype", "PATIENT")
                .build();
        Response response = client.newCall(request).execute();

        int code = response.code();

        if (code == 550) {
            return Constants.AUTH_TOKEN_REFRESH_REQUIRED;
        }
        if (code == 450) {
            return null;
        }

        String result = response.body().string();
        Log.d("test content", result);
        return result;
    }
}
