package com.healthapp.drgsgupta.NaviaContentActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eftimoff.viewpagertransformers.AccordionTransformer;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.healthapp.drgsgupta.R;
import com.healthapp.drgsgupta.ViewDot;
import com.healthapp.drgsgupta.ZoomOutPageTransformer;
import com.healthapp.drgsgupta.VideoListActivity.VideoListActivity;
import com.navia.naviahelp.BundleConstants;
import com.navia.naviahelp.Constants;
import com.navia.naviahelp.HelperFunctions;
import com.navia.naviahelp.MyPrefs;

import java.util.ArrayList;


/**
 * Created by amitgupta on 12/14/17.
 */

public class NaviaContentActivity extends Fragment implements AllContentActivityView,
        SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    ViewPager viewPager, viewPagerVideo, viewPagerTip;
    ShimmerFrameLayout shimmerProgressBlogs, shimmerProgressVideos, shimmerProgressTip;
    NaviaContentActivityPresenter mPresenter;
//    SwipeRefreshLayout mSwipeLayout;
    RelativeLayout blogsProgressView, blogsVideoView, tipsProgressView;
    TextView tv_view_all_videos, tv_view_all_blogs, tv_view_all_tips;
    TextView tv_empty_blogs, tv_empty_videos, tv_empty_tips;
    Activity mActivity;
    ViewDot indicatorBlogs, indicatorVideos;
    RelativeLayout no_internet_layout;
    CardView card_refresh;

    String phoneNumber;
    String authToken;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_content, container, false);

        mActivity = getActivity();

        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        viewPagerVideo = (ViewPager) view.findViewById(R.id.viewpager_video);
        viewPagerTip = (ViewPager) view.findViewById(R.id.viewpager_tip);


        shimmerProgressVideos = (ShimmerFrameLayout) view.findViewById(R.id.placeholder_videos);
        shimmerProgressBlogs = (ShimmerFrameLayout) view.findViewById(R.id.placeholder_blogs);
        shimmerProgressTip = (ShimmerFrameLayout) view.findViewById(R.id.placeholder_tip);

        blogsProgressView = (RelativeLayout) view.findViewById(R.id.progress_view_blogs);
        blogsVideoView = (RelativeLayout) view.findViewById(R.id.progress_view_video);
        tipsProgressView = (RelativeLayout) view.findViewById(R.id.progress_view_tip);

        tv_view_all_blogs = (TextView) view.findViewById(R.id.tv_view_all_blogs);
        tv_view_all_videos = (TextView) view.findViewById(R.id.tv_view_all_videos);
        tv_view_all_tips = (TextView) view.findViewById(R.id.tv_view_all_tips);
        indicatorBlogs = (ViewDot) view.findViewById(R.id.view_dot_blogs);
        indicatorVideos = (ViewDot) view.findViewById(R.id.view_dot_videos);

        tv_empty_blogs = (TextView) view.findViewById(R.id.tv_empty_blogs);
        tv_empty_videos = (TextView) view.findViewById(R.id.tv_empty_videos);
        tv_empty_tips = (TextView) view.findViewById(R.id.tv_empty_tips);


        tv_view_all_videos.setOnClickListener(this);
        tv_view_all_blogs.setOnClickListener(this);
        tv_view_all_tips.setOnClickListener(this);

        no_internet_layout = (RelativeLayout) view.findViewById(R.id.no_internet_layout);
        card_refresh = (CardView) view.findViewById(R.id.card_refresh);
        card_refresh.setOnClickListener(this);


//        mSwipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_to_refresh);
//        mSwipeLayout.setOnRefreshListener(this);

        mPresenter = new NaviaContentActivityPresenter(this);

        phoneNumber = MyPrefs.getString(getContext(), Constants.PHONE_ID, "");
        authToken = MyPrefs.getString(getContext(), Constants.AUTH_TOKEN, "");

        mPresenter.loadBlogs(authToken, phoneNumber);

        return view;
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_view_all_blogs) {
            mPresenter.onViewAllBlogsClicked();
        } else if (v.getId() == R.id.tv_view_all_videos) {
            mPresenter.onViewAllVideosClicked();
        } else if (v.getId() == R.id.tv_view_all_tips) {
            mPresenter.onViewAllTipsClicked();
        } else if (v.getId() == R.id.card_refresh) {
            no_internet_layout.setVisibility(View.GONE);
            mPresenter.loadBlogs(authToken, phoneNumber);
        }

    }

    @Override
    public void onRefresh() {
        mPresenter.loadBlogs(authToken, phoneNumber);
    }

    @Override
    public void showProgress() {
        shimmerProgressBlogs.startShimmerAnimation();
        shimmerProgressVideos.setAutoStart(true);
        shimmerProgressTip.startShimmerAnimation();
    }

    @Override
    public void hideProgress() {
        shimmerProgressBlogs.stopShimmerAnimation();
        shimmerProgressVideos.stopShimmerAnimation();
        shimmerProgressTip.stopShimmerAnimation();
        blogsVideoView.setVisibility(View.GONE);
        blogsProgressView.setVisibility(View.GONE);
        tipsProgressView.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
//        mSwipeLayout.setRefreshing(false);
        if (mActivity!=null) {
            if (isAdded()) {
                HelperFunctions.displayDialog(mActivity, getString(R.string.SomethingWentWrong));
                shimmerProgressBlogs.stopShimmerAnimation();
                shimmerProgressVideos.stopShimmerAnimation();
                shimmerProgressTip.stopShimmerAnimation();
            }
        }
    }

    @Override
    public void showErrorString(String error) {
//        mSwipeLayout.setRefreshing(false);
        if (mActivity!=null) {
            if (isAdded()) {
                HelperFunctions.displayDialog(mActivity, error);
                shimmerProgressBlogs.stopShimmerAnimation();
                shimmerProgressVideos.stopShimmerAnimation();
                shimmerProgressTip.stopShimmerAnimation();
            }
        }
    }

    @Override
    public void showVideos(ArrayList<VideoItem> arrayList) {
        viewPagerVideo.setAdapter(new ViewPagerAdapterVideo(mActivity, arrayList));
        viewPagerVideo.setPageTransformer(false, new AccordionTransformer());
        indicatorVideos.setViewPager(viewPagerVideo);
    }

    @Override
    public void showBlogs(ArrayList<ArticleItem> articleItems) {
        viewPager.setAdapter(new ViewPagerAdapterContent(mActivity, articleItems));
        indicatorBlogs.setViewPager(viewPager);
        viewPager.setPageTransformer(false, new AccordionTransformer());
//        mSwipeLayout.setRefreshing(false);
    }

    @Override
    public void showTips(ArrayList<ArticleItem> tips) {
        viewPagerTip.setAdapter(new ViewPagerTip(mActivity, tips));
        viewPagerTip.setPageTransformer(false, new ZoomOutPageTransformer());
    }

    @Override
    public void openBlog(ArticleItem blogItem) {
        Intent intent = new Intent(mActivity, ContentActivity.class);
        intent.putExtra(BundleConstants.BUNDLE_ID_ARTICLE, blogItem.getId());
        intent.putExtra(BundleConstants.BUNDLE_IMAGE_ARTICLE, blogItem.getImg_url());
        intent.putExtra(BundleConstants.BUNDLE_CONTENT_ARTICLE, blogItem.getContent());
        intent.putExtra(BundleConstants.BUNDLE_DATE_ARTICLE, blogItem.getDate());
        intent.putExtra(BundleConstants.BUNDLE_TITLE_ARTICLE, blogItem.getHeading());
        startActivity(intent);
    }

    @Override
    public void openVideo(VideoItem videoItem) {
        Intent intent = new Intent(mActivity, YoutubeActivity.class);
        intent.putExtra(BundleConstants.BUNDLE_VIDEO_URL, videoItem.getVideoUrl().split("v=")[1].substring(0, 11));

        startActivity(intent);

    }

    @Override
    public void openVideosActivity() {
        Intent intent = new Intent(mActivity, VideoListActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_left, R.anim.slide_out_right);
    }

    @Override
    public void openContentActivity() {
        Intent intent = new Intent(mActivity, ContentListActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_left, R.anim.slide_out_right);
    }

    @Override
    public void openTipsActivity() {
        Intent intent = new Intent(mActivity, TipsActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_left, R.anim.slide_out_right);
    }

    @Override
    public void showEmptyBlogs() {
        tv_view_all_blogs.setVisibility(View.INVISIBLE);
        tv_empty_blogs.setVisibility(View.VISIBLE);
    }

    @Override
    public void showEmptyVideos() {
        tv_view_all_videos.setVisibility(View.INVISIBLE);
        tv_empty_videos.setVisibility(View.VISIBLE);
    }

    @Override
    public void showEmptyTips() {
        tv_view_all_tips.setVisibility(View.INVISIBLE);
        tv_empty_tips.setVisibility(View.VISIBLE);
    }


    @Override
    public void showNoInternetConnectionError() {
        no_internet_layout.setVisibility(View.VISIBLE);
    }
}
