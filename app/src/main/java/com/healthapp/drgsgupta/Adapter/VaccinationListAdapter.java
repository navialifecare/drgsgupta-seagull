package com.healthapp.drgsgupta.Adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;
import com.healthapp.drgsgupta.VaccinationSchedulesClass;
import com.healthapp.drgsgupta.VaccineCalendar;
import com.healthapp.drgsgupta.task.VaccinationItem;
import com.navia.naviahelp.Constants;

import java.util.ArrayList;

/**
 * Created by JaiGuruji on 22/02/17.
 */

public class VaccinationListAdapter extends RecyclerView.Adapter<VaccinationListAdapter.MyViewHolder> {
    Context mActivity;
    ArrayList<VaccinationItem> mVaccinationList;
    RecyclerView mRecyclerView;
    LayoutInflater mInflater;


    public VaccinationListAdapter(ArrayList<VaccinationItem>arrayList,Activity context,RecyclerView recyclerView) {
        mActivity=context;
        mVaccinationList=arrayList;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRecyclerView=recyclerView;
    }

    @Override
    public int getItemCount() {
        return mVaccinationList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = mInflater.inflate(R.layout.item_vaccination_list,parent,false);
        view.setOnClickListener(new MyOnClickListener());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
      holder.name.setText(mVaccinationList.get(position).getmBabyName());
      holder.date.setText(mVaccinationList.get(position).getmStartDate());
      holder.doctor_name.setText(mVaccinationList.get(position).getmDoctorName());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView name, date;
        public ImageView edit;
        TextView doctor_desc,doctor_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            name=(TextView) itemView.findViewById(R.id.treatName);
            date = (TextView) itemView.findViewById(R.id.dateText);
            doctor_desc=(TextView)itemView.findViewById(R.id.doctor_tv_desc);
            doctor_name=(TextView)itemView.findViewById(R.id.doctor_tv_main);
        }
    }


    class MyOnClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
           int pos = mRecyclerView.getChildLayoutPosition(v);
            Fragment fragment = new VaccineCalendar();
            FragmentManager fragmentManager = ((VaccinationSchedulesClass)mActivity).getSupportFragmentManager();
            Bundle bundle =  new Bundle();
            bundle.putString("jsonArray",mVaccinationList.get(pos).getJsonArray()+"");
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.placeholder,fragment).
                    addToBackStack(Constants.VaccinationCalenderFragment).commit();
        }
    }


}
