package com.healthapp.drgsgupta.Adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.healthapp.drgsgupta.R;
import com.navia.naviahelp.Constants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by JaiGuruji on 23/02/17.
 */

public class MyVaccinationListAdapter extends BaseExpandableListAdapter {
    Context mContext;
    JSONArray mData = new JSONArray();
    private LayoutInflater mInflater;
    Typeface type;

    public  MyVaccinationListAdapter(Context context, JSONArray jsonArray) {
        mContext = context;
        mData = jsonArray;

    }

    @Override
    public int getGroupCount() {
        return mData.length();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return ((JSONObject) mData.getJSONObject(groupPosition)).getJSONArray("tasks").length();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        try {
            return ((JSONObject) mData.getJSONObject(groupPosition)).getString("date");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        try {
            return ((JSONObject) mData.getJSONObject(groupPosition)).getJSONArray("tasks").getJSONObject(childPosition);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        GroupHolder groupHolder = null;

        if (convertView == null) {
            groupHolder = new GroupHolder();
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.my_treatment_adapter, null);
            groupHolder.textView = (TextView) convertView
                    .findViewById(R.id.date);
            //  groupHolder.textView.setTypeface(type);
            groupHolder.img = (ImageView) convertView.findViewById(R.id.group_arrow);
            convertView.setTag(groupHolder);
        } else {
            groupHolder=(GroupHolder)convertView.getTag();
        }


        if (isExpanded) {
            groupHolder.img.setImageResource(R.drawable.group_up);
        } else {
            groupHolder.img.setImageResource(R.drawable.group_down);
        }

        try {
            groupHolder.textView.setText(((JSONObject) mData.getJSONObject(groupPosition)).getString("date"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_vaccination_detail, null);
        }

        fillDataInView(groupPosition, childPosition, convertView);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


    public class GroupHolder {
        ImageView img;
        TextView textView;
    }


    private void fillDataInView(int groupPosition, int childPosition, View convertView) {
        TextView medicine_name = (TextView) convertView.findViewById(R.id.medicine_name);
        TextView status = (TextView) convertView.findViewById(R.id.status);
        TextView time = (TextView) convertView.findViewById(R.id.time);
        LinearLayout info = (LinearLayout) convertView.findViewById(R.id.info_med);
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.mytreatemnt_list_item_linear_layout);
        try {
            JSONObject data = (JSONObject) mData.getJSONObject(groupPosition);
            JSONArray entities = data.getJSONArray("tasks");
            JSONObject entity = entities.getJSONObject(childPosition);
            medicine_name.setText(entity.getString("vaccination_name"));
            status.setText(entity.getString("taken_status"));
            time.setText(entity.getString("due_time"));
            time.setVisibility(View.GONE);
            info.setVisibility(View.GONE);
            linearLayout.setBackground(mContext.getResources().getDrawable(R.drawable.bg_card));
            // putting color in LinearLayout
            int desiredColor = Color.BLACK;
            if (entity.getString("color").equals("green")) {
                linearLayout.setBackgroundColor(Color.parseColor("#8dc63f"));
                desiredColor = Color.BLACK;
            } else if (entity.getString("color").equals("red")) {
                linearLayout.setBackgroundColor(Color.parseColor("#FF6666"));
                desiredColor = Color.WHITE;
            } else if (entity.getString("color").equals("yellow")) {
                linearLayout.setBackgroundColor(Color.YELLOW);
                desiredColor = Color.BLACK;
            }


//            final String medicineName = entity.getString("medicine_name");
            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // HelperFunctions.logMessage(medicineName);
                    JSONObject json = new JSONObject();
                    String URL = Constants.URL + "get_medicine_details_given_name";
                    try {
       //                 json.put("name", medicineName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    GetSideEffectsTask getSideEffectsTask = new
                             GetSideEffectsTask(json.toString(), URL, mContext);
                    getSideEffectsTask.execute("");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class GetSideEffectsTask extends AsyncTask<String, Integer, JSONObject> {
        private String url;
        //  private List<NameValuePair> nameValuePairs;
        private String nameValuePairs;
        ProgressDialog dialog;
        Context context;


        public GetSideEffectsTask(String nameValuePairs, String url, Context c) {
            this.url = url;
            this.nameValuePairs = nameValuePairs;
            context = c;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Please wait...");
            dialog.setIndeterminate(true);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject exam = postData(url, nameValuePairs);
            if (exam != null)
                return exam;
            else return null;
        }

        @Override
        protected void onPostExecute(JSONObject exam) {

            dialog.dismiss();
            super.onPostExecute(exam);
            if (exam != null) {

                System.out.println(exam);
                String genericName = "NA";
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.side_effects_layout, null);

                try {
                    genericName = exam.getString("generic_name");
                    if (!exam.getString("precaution").equals("")) {
                        TextView tv_precaution = (TextView) v.findViewById(R.id.tv_precaution);
                        tv_precaution.setText(exam.getString("precaution"));
                    }
                    if (!exam.getString("indication").equals("")) {
                        TextView tv_indication = (TextView) v.findViewById(R.id.tv_indication);
                        tv_indication.setText(exam.getString("indication"));
                    }
                    if (!exam.getString("contra_indication").equals("")) {
                        TextView tv_contra_indication = (TextView) v.findViewById(R.id.tv_contra_indication);
                        tv_contra_indication.setText(exam.getString("contra_indication"));
                    }
                    if (!exam.getString("side_effects").equals("")) {
                        TextView tv_side_effects = (TextView) v.findViewById(R.id.tv_side_effects);
                        tv_side_effects.setText(exam.getString("side_effects"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                builder.setView(v);

                TextView title = new TextView(context);
                // You Can Customise your Title here
                title.setText("Generic Name: " + genericName);
                title.setBackgroundColor(Color.DKGRAY);
                title.setPadding(10, 10, 10, 10);
                title.setGravity(Gravity.CENTER);
                title.setTextColor(Color.WHITE);
                title.setTextSize(20);
                builder.setCustomTitle(title);
                builder.show();
            } else { }
        }


        public JSONObject postData(String url, String nameValuePairs) {
            // Create headingheader_box new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);
            JSONObject js = null;

            try {
                httppost.setEntity(new StringEntity(nameValuePairs));
                StringEntity se = new StringEntity(nameValuePairs);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                String responsestring = EntityUtils.toString(entity);
                js = new JSONObject(responsestring);


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return js;
        }
    }
}
