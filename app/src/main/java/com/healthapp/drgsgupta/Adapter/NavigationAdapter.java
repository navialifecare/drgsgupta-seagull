package com.healthapp.drgsgupta.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthapp.drgsgupta.FontCache;
import com.healthapp.drgsgupta.FontNamesClass;
import com.healthapp.drgsgupta.R;

import java.util.List;

/**
 * Created by amitgupta on 10/11/17.
 */

public class NavigationAdapter extends ArrayAdapter<String> {

    Context ctx;
    int Resource;
    List<String> obj;
    int images1[];
    LayoutInflater inflater;
    Typeface typeface;


    public NavigationAdapter(Context context, int resource, List<String> objects, int[] images) {
        super(context, resource, objects);
        ctx = context;
        Resource = resource;
        obj = objects;
        images1 = images;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = FontCache.getTypeFace(FontNamesClass.fontSemibold,context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(Resource, null, false);
            //        type = Typeface.createFromAsset(getActivity().getAssets(),"robotoMedium.ttf");
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.textview_fragment);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView_fragment);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textView.setText(obj.get(position));
        holder.textView.setTypeface(typeface);
        holder.imageView.setImageResource(images1[position]);
        return convertView;
    }

    private class ViewHolder {
        TextView textView;
        ImageView imageView;
    }
}

