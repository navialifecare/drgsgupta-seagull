package com.healthapp.drgsgupta;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthapp.appointment_module.AppointmentDelegate;
import com.healthapp.appointment_module.AppointmentsListCallback;
import com.healthapp.broadcast_module.BroadcastDelegate;
import com.healthapp.broadcast_module.BroadcastHistoryMessagesActivity;
import com.healthapp.chat_module.XmppChatCallback;
import com.healthapp.chat_module.XmppChatDelegate;
import com.healthapp.doctor_departments_module.DoctorsDepartmentsCallback;
import com.healthapp.doctor_departments_module.DoctorsDepartmentsDelegate;
import com.healthapp.doctor_departments_module.LinkedDoctorsList.ActionForLinkedDoctor;
import com.navia.naviahelp.HelperFunctions;

import es.dmoral.toasty.Toasty;

import static com.healthapp.drgsgupta.AppConstants.VERSION_CODE;

/**
 * Created by amitgupta on 8/28/17.
 */

public class MessageOptionsFragment extends AppCompatActivity implements View.OnClickListener{

    Activity mActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_message_options);

        mActivity = this;

        ImageView img_back = findViewById(R.id.img_back);
        TextView tv_title = (TextView)findViewById(R.id.tv_title);
        tv_title.setTypeface(FontCache.getTypeFace(FontNamesClass.fontSemibold,mActivity));
        TextView tv_message = (TextView)findViewById(R.id.tv_text);
        TextView tv_broadcast = (TextView)findViewById(R.id.tv_broadcast);
        tv_message.setTypeface(FontCache.getTypeFace(FontNamesClass.fontSemibold,mActivity));
        tv_broadcast.setTypeface(FontCache.getTypeFace(FontNamesClass.fontSemibold,mActivity));


        CardView card_text = (CardView)findViewById(R.id.card_message);
        CardView card_broadcast = (CardView)findViewById(R.id.card_broadcast);
        img_back.setOnClickListener(this);
        card_text.setOnClickListener(this);
        card_broadcast.setOnClickListener(this);
        img_back.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.img_back){
            finish();
        }
        else if(v.getId()==R.id.card_message){
            XmppChatDelegate.getInstance(AppConstants.APP_NAME, VERSION_CODE).openChatWithDoctor(AppConstants.MEDICAL_DIRECTOR_PHONE_NUMBER,
                    AppConstants.MEDICAL_DIRECTOR_NAME, mActivity);
        }
        else if(v.getId()==R.id.card_broadcast){
            BroadcastDelegate.getInstance(getIntent().getStringExtra("appName"),getIntent().getStringExtra("versionCode")).startBroadcastModule(mActivity);
        }
    }
}
